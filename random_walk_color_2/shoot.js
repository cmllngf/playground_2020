class Shoot {
  constructor(step = 50) {
    this.shooting = false
    this.xstep = 0
    this.ystep = 0
    this.step = step
    this.currentStep = 0
    this.justFinished = false
    this.x1 = 0
    this.y1 = 0
    this.x2 = 0
    this.y2 = 0
  }

  shoot(x1, y1, x2, y2) {
    if(!this.shooting) {
      let xdiff = x2 - x1
      let ydiff = y2 - y1
      this.xstep = xdiff / this.step
      this.ystep = ydiff / this.step
      this.shooting = true
      this.x1 = x1
      this.y1 = y1
      this.x2 = x2
      this.y2 = y2
    }
  }

  update(f) {
    this.justFinished = false
    if(this.shooting) {
      this.currentStep ++
      // stroke(0)
      // strokeWeight(10)
      // point(this.x1 + this.xstep * this.currentStep, this.y1 + this.ystep * this.currentStep)
      f(this.x1 + this.xstep * this.currentStep, this.y1 + this.ystep * this.currentStep)
    }

    if(this.currentStep >= this.step) {
      this.justFinished = true
      this.currentStep = 0
      this.shooting = false
    }
  }
}