let visited = [];
let colors = [];
let walkers = []
let tries = 0
let stop = false
let x = 0
let y = 0
const amplitude = 100

const k = 300
const pixel_size = 2;
const cwidth = 700;
const cheight = 700;

let t = 0
let texture

function seed(s, c) {
  visited[s.y * width + s.x] = true;
  colors[s.y * width + s.x] = c;
}

function preload() {
  texture = loadImage('./assets/texture_1.png')
}

function setup() {
  createCanvas(cwidth, cheight)
  background('white');
  loadPixels()
  // image(texture, 0 , 0)
}

function draw() {
  if(!stop) {
    // y = int(sin(t) * amplitude + height/2)
    x = int(random(width))
    y = int(random(height))
    createWalker(x, y)
  }
  stop = true

  for(i = 0; i < 1000; i++) {
    walkers.forEach((walker) => {
      walker.update(visited, colors, pixel_size)
    })
  }

  walkers = walkers.filter((walker) => walker.alive)
}

function createWalker(x = int(random(width)), y = int(random(height)), lifespan = -1) {
  if(tries >= k) {
    stop = true
  }
  if(visited[y * width + x]) {
    tries ++
    return;
  }
  tries = 0
  p = createVector(x, y)
  r = random(255)
  g = random(255)
  b = random(255)
  c = color(texture.get(x,y))
  seed(p, c)
  walkers.push(new Walker(p, lifespan))
}

function keyPressed(key) {
  if(key.keyCode === 80)
    save()
}

