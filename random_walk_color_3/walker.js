class Walker {
    constructor(p, lifespan = -1) {
        this.startP = p
        this.unprocessed_points = [p]
        this.alive = true
        this.lifespan = lifespan
        this.life = 0
    }

    isValid(p) {
        return p.x >= 0 && p.x < width && p.y >= 0 && p.y < height
    }

    gaussian_color(c, colorAim) {
        const step = 5
        const r = randomGaussian(red(c), random(1, 1)) + (colorAim ? red(c) < red(colorAim) ? step : -step : 0)
        const g = randomGaussian(green(c), random(1, 2)) + (colorAim ? green(c) < green(colorAim) ? step : -step : 0)
        const b = randomGaussian(blue(c), random(1, 2)) + (colorAim ? blue(c) < blue(colorAim) ? step : -step : 0)
        return color(r,g,b)
    }

    update(visited, colors, pixel_size) {
        if(this.unprocessed_points.length === 0 ||
            this.lifespan !== -1 && this.life >= this.lifespan) {
            this.alive = false;
            return;
        }
        this.life ++;
        const random_index = int(random(this.unprocessed_points.length));
        const random_point = this.unprocessed_points[random_index];
        this.unprocessed_points.splice(random_index, 1);

        for(let y = random_point.y - 1; y <= random_point.y + 1; y++) {
          for(let x = random_point.x - 1; x <= random_point.x + 1; x++) {
            if(y == random_point.y && x == random_point.x) 
              continue;
    
            if(visited[y * width + x]) 
              continue;
            
            if(!this.isValid(createVector(x, y)))
              continue;
            
            visited[y * width + x] = true;
            this.unprocessed_points.push(createVector(x,y))
            colors[y * width + x] = this.gaussian_color(colors[random_point.y * width + random_point.x], color(texture.get(x,y)))
            stroke(colors[y * width + x])
            strokeWeight(pixel_size)
            line(random_point.x, random_point.y, x,y)
          }
        }
    }
}