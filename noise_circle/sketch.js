let xoff = 0;
let yoff = 0;
let t = 0

const circles = 10
const radius = 200

let seed
let circleColor

function setup() {
  createCanvas(800, 800)
  colorMode(HSB);
  seed = random(10000)
  circleColor = color(random(360), random(20, 70), random(40, 90))
}

// cubes running in circle
function draw() {
  randomSeed(seed)
  background(0);

  translate(width / 2, height / 2)
  rotate(HALF_PI)

  beginShape()
  strokeWeight(1)
  circleColor.setAlpha(.03)
  stroke(circleColor)
  noFill()
  let x = random(-width/2, width/2);
  let y = random(-height/2, height/2);
  const j = 9;
  for (let i = 0; i < 30000; i++) {
    curveVertex(x, y)
    const qx = random() < .5 ? -1 : 1;
    const qy = random() < .5 ? -1 : 1;
    x += qx * j;
    y += qy * j;
    x = constrain(x, -width/2, width/2);
    y = constrain(y, -height/2, height/2);
  }
  endShape()
  circleColor.setAlpha(1)

  fill(100, .01)
  stroke(circleColor)
  strokeWeight(1)
  beginShape()
  const numberOfV = 60
  for (let i = 0; i < numberOfV; i++) {
    const phi = (TWO_PI / numberOfV) * i
    // const noiseEval = noise(cos(phi), sin(i * PI))
    // const noiseR = map(noiseEval, 0, 1, -25, 25)
    // vertex(cos(phi) * radius + noiseR, sin(phi) * radius + noiseR)
    // curveVertex(cos(phi) * radius + noiseR, sin(phi) * radius + noiseR)
    curveVertex(cos(phi) * radius, sin(phi) * radius)
  }
  endShape(CLOSE)
  
  for (let j = 0; j < circles; j++) {
    const r = (radius / circles) * (j + 1)
    for (let i = 0; i < r; i++) {
      const phi = (TWO_PI / r) * i;   
      const noiseEval = noise(cos(phi / 2) * 2, sin(i * PI) * 5, t)
      const noiseR = map(noiseEval, 0, 1, -300, 300)
      stroke(180 + random (-150, 50))
      strokeWeight(random(1, 5))
      point(cos(phi) * (r + noiseR), sin(phi) * (r + noiseR))
    }
  }
  t += .005
  noLoop()
}

function myNoise(value) {
  const p = int(value % 5)
  return pow(sin(value), p)
}

function keyPressed(key) {
  if(key.keyCode === 80)
    save()
}