// Make as much different circles as possible
const width = 900, height = 900;
const mapWidth = 2, mapHeight = 2;
const marginAroundCircles = 25;
const tileWidth = (width/mapWidth)
const tileheight = (height/mapHeight);
let seed;

function setup() {
  createCanvas(width, height)
  background('black')
  colorMode(HSB)
  seed = random(999)
}

function draw() {
  randomSeed(seed)
  for (let y = 0; y < mapHeight; y++) {
    for (let x = 0; x < mapWidth; x++) {
      circle_9(x * tileWidth + (tileWidth/2), y * tileheight + (tileheight/2), tileWidth - marginAroundCircles*2)
    }
  }
  noLoop()
}

function circle_1(x, y, r) {
  noStroke()
  fill(random(0,200))
  circle(x, y, r)
}

function circle_2(x, y, r) {
  noFill()
  let pr;
  let c = color(random(360), random(50,80), random(40,80))
  for(let i = 0; i < 1000; i++) {
    do {
      pr = random(0, r/2)
    }while(pr > r/2);
    const a = random(TWO_PI)
    stroke(c)
    point(x + cos(a) * pr, y + sin(a) * pr)
  }
}

function circle_3(x, y, r) {
  noFill()
  stroke(255)
  const step = TWO_PI / 100;
  for(let i = 0; i < TWO_PI; i+=step) {
    const mag = random(r/2)
    line(x,y,x+cos(i)*mag,y+sin(i)*mag)
  }
}

function circle_4(x, y, r) {
  noFill()
  const numberOfCircles = 20;
  const step = r/numberOfCircles/2;
  const angleStep = TWO_PI / 200
  for(let i = 0; i < numberOfCircles; i++) {
    for(let a = 0; a < TWO_PI; a+=angleStep) {
      stroke(random(50))
      point(x + cos(a) * i * step, y + sin(a) * i * step)
    }
  }
}

function circle_5(x, y, r) {
  noFill()
  stroke(50)
  const seedOff = random(999999)
  const angleStep = TWO_PI / 200
  for(let i = 0; i < 5; i++) {
    const noiseMax = 5 * (i * 2 + 1)
    beginShape()
    for(let a = 0; a < TWO_PI; a+=angleStep) {
      const xoff = map(cos(a), -1, 1, 0, noiseMax)
      const yoff = map(sin(a), -1, 1, 0, noiseMax)
      const d = map(noise(xoff, yoff, seedOff), 0, 1, r/2-10,r/2+10)
      vertex(x + cos(a) * d, y + sin(a) * d)
    }
    endShape(CLOSE)
  }
}

// needs work
function circle_6(x, y, r) {
  stroke(50)
  beginShape()
  for(let i = -r/2; i < r/2; i+=0.5) {
    const multiplier = map(abs(i), 0, r/2, r/2, 0)
    yy = sin(i)
    vertex(x + i, y + sin(i) * multiplier)
  }
  endShape()
}

function circle_7(x, y, r) {
  stroke(0)
  noFill()
  push();
  translate(x, y)
  numberOfCircle = 1200
  circle(0, 0, r)
  for (let i = 0; i < numberOfCircle; i++) {
    rotate(random(TWO_PI))
    centerY = random(r/2)
    circle(0, centerY, r - (centerY * 2))
  }
  pop();
}

function circle_8(x, y, r) {
  circle_8_recursive(x, y, r, 0);
}

function circle_8_recursive(x, y, r, d) {
  if(d > 50)
    return;
  push();
  translate(x, y);
  rotate(random(TWO_PI))
  steps = [-4,4,0,0];
  currentPoint = createVector(0, 0)
  noFill()
  stroke(0)
  while(dist(currentPoint.x, currentPoint.y, 0, 0) < r/2) {
    point(currentPoint.x, currentPoint.y);

    currentPoint.x += steps[int(random(steps.length))]
    if(currentPoint.x != 0)
      currentPoint.y += steps[int(random(steps.length - 2))]      
  }
  pop();
  circle_8_recursive(x,y,r,d+1);
}

function circle_9(x, y, r) {
  circle_9_recursive(x, y, r, 0);
}

function circle_9_recursive(x, y, r, d) {
  if(d > 50)
    return;
  push();
  translate(x, y);
  rotate(random(TWO_PI))
  steps = [-7,7,0,0];
  currentPoint = createVector(0, 0)
  previousPoint = currentPoint
  noFill()
  stroke(255)
  while(dist(currentPoint.x, currentPoint.y, 0, 0) < r/2) {
    currentPoint.x += steps[int(random(steps.length))]
    if(currentPoint.x != 0)
      currentPoint.y += steps[int(random(steps.length - 2))]
    
    line(currentPoint.x, currentPoint.y, previousPoint.x, previousPoint.y);
    previousPoint = currentPoint
  }
  pop();
  circle_9_recursive(x,y,r,d+1);
}