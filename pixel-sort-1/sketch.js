const cwidth = 150;
const cheight = 250;

const scl = 5;

const iterations = 150;
let ite = 0
let img

function preload() {
  img = loadImage('./image.png')
}

function setup() {
  createCanvas(cwidth, cheight)
  background(0)
  // colorMode(HSB)

  // for (let i = 0; i < cwidth; i+=scl) {
  //   for (let j = 0; j < cheight; j+=scl) {
  //     noStroke()
  //     fill(random(360), 30, 70)
  //     square(i,j,scl)
  //   }
  // }
  // image(img, 0, 0)
}

function draw() {
  if(ite >= iterations) {
    noLoop();
    console.log('done.')
    image(img, 0, 0)
    return;
  }

  for (let i = 0; i < cwidth; i+=scl) {
    for (let j = 0; j < cheight; j+=scl) {
      processPair(i, j, i, j+1)
    }
  }
  ite ++
}

function processPair(x1, y1, x2, y2) {
  const pixels = img.get(x1, y1)
  const pixels2 = img.get(x2, y2)
  const p1a = pixels.reduce((acc, cur) => acc + cur,0)/3
  const p2a = pixels2.reduce((acc, cur) => acc + cur,0)/3
  if(p2a > p1a) {
    // console.log(p2a, p1a)
    img.set(x1, y1, color(pixels2))
    img.set(x2, y2, color(pixels))
    img.updatePixels();
  }
}

function keyPressed(key) {
  if(key.keyCode === 80)
    save()
}

// Sharpen function from Mike Cao
// function sharpen(ctx, w, h, mix) {
//   var x, sx, sy, r, g, b, a;
//   var dstOff, srcOff, wt, cx, cy, scy, scx;
//   var weights = [0, -1, 0, -1, 5, -1, 0, -1, 0],
//     katet = Math.round(Math.sqrt(weights.length)),
//     half = (katet * 0.5) | 0,
//     dstData = ctx.createImageData(w, h),
//     dstBuff = dstData.data,
//     srcBuff = ctx.getImageData(0, 0, w, h).data,
//     y = h;

//   while (y--) {
//     x = w;
//     while (x--) {
//       sy = y;
//       sx = x;
//       dstOff = (y * w + x) * 4;
//       r = 0;
//       g = 0;
//       b = 0;
//       a = 0;

//       for (cy = 0; cy < katet; cy++) {
//         for (cx = 0; cx < katet; cx++) {
//           scy = sy + cy - half;
//           scx = sx + cx - half;

//           if (scy >= 0 && scy < h && scx >= 0 && scx < w) {
//             srcOff = (scy * w + scx) * 4;
//             wt = weights[cy * katet + cx];

//             r += srcBuff[srcOff] * wt;
//             g += srcBuff[srcOff + 1] * wt;
//             b += srcBuff[srcOff + 2] * wt;
//             a += srcBuff[srcOff + 3] * wt;
//           }
//         }
//       }

//       dstBuff[dstOff] = r * mix + srcBuff[dstOff] * (1 - mix);
//       dstBuff[dstOff + 1] = g * mix + srcBuff[dstOff + 1] * (1 - mix);
//       dstBuff[dstOff + 2] = b * mix + srcBuff[dstOff + 2] * (1 - mix);
//       dstBuff[dstOff + 3] = srcBuff[dstOff + 3];
//     }
//   }

//   sortedContext.putImageData(dstData, 0, 0);
// }

