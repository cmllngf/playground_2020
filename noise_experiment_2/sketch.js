let seed;
let zoff = 0

const scl = 1
const noiseMultiplier = 255;
const xstep = 0.001
const ystep = 0.01

function setup() {
  createCanvas(900, 900)
  seed=random(10000)
  randomSeed(seed)
}

function draw() {
  background(0, 50);
  stroke(255, 20)
  for (let x = 0; x < width; x++) {
    for (let y = 0; y < 10; y++) {
      const py = map(noise(x * xstep, y*ystep, zoff), 0, 1, -200, 200)
      point(x, height/2 + py)
    }
  }
  zoff+=0.01;
}

function keyPressed(key) {
  if(key.keyCode === 80)
    saveCanvas(canvas, 'noise_noise_'+seed, 'png')
}
