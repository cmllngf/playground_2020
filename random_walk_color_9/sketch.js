let visited = [];
let colors = [];
let x = 0
let y = 0

const k = 300
const pixel_size = 3;
const cwidth = 560;
const cheight = 560;
const border = 100;
const countH = 10
const countV = 10

const poisson_radius = 10
const poisson_k = 2

let currentSystemIndex = 0
const systems = []

function seed(s, c) {
  visited[s.y * width + s.x] = true;
  colors[s.y * width + s.x] = c;
}

function setup() {
  createCanvas(cwidth, cheight)
  colorMode(HSB)
  background(255)
  for (let x = 0; x < 5; x++) {
    for (let y = 0; y < 5; y++) {
      systems.push(new System(10 + x * 110, 10 + y * 110, 100))
    }
  }
}

function draw() {
  if(!systems[currentSystemIndex].update()) {
    currentSystemIndex++
    if(!systems[currentSystemIndex]) {
      noLoop();
    }
  }
  
}

function keyPressed(key) {
  console.log(key)
  if(key.keyCode === 80)
    saveCanvas(canvas, 'random_walk_color', 'png')
}
