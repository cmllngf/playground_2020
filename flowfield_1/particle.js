class Particle {
  // If maxLifetime is 0, then it doesn't die
  constructor(type = 'dot', maxLifetime = 0) {
    this.pos = createVector(random(width), random(height))
    this.prevpos = createVector(random(width), random(height))
    this.vel = createVector(0,0)
    this.acc = createVector(0,0)
    this.maxSpeed = 4;
    this.lifetime = 0;
    this.id = random(99999)
    this.saturation = random(100)
    this.brightness = random(100)
    this.maxLifetime = maxLifetime
    this.type = type
  }
  
  update() {
    this.vel.add(this.acc)
    this.vel.limit(this.maxSpeed);
    this.prevpos = this.pos.copy();
    this.pos.add(this.vel)
    this.acc.mult(0)
    this.edges()
    this.lifetime++;
  }

  applyForce(force) {
    this.acc.add(force)
  }

  follow(flowfield) {
    const x = floor(this.pos.x / scl)
    const y = floor(this.pos.y / scl)
    const vector = flowfield[y * cols + x]
    this.applyForce(vector)
  }
  
  display() {
    if(this.maxLifetime == 0 || this.lifetime < this.maxLifetime) {
      noFill()
      strokeWeight(1)
      const hsba = color(noise(this.lifetime, this.id) * 360, this.saturation,this.brightness,1)
      stroke(hsba)
      line(this.pos.x, this.pos.y, this.prevpos.x, this.prevpos.y)
    }
  }

  edges() {
    if(this.pos.x < 0){
      this.pos.x = width
      this.prevpos = this.pos.copy();
    }
    if(this.pos.x > width)
    {
      this.pos.x = 0
      this.prevpos = this.pos.copy();
    }
    if(this.pos.y < 0)
    {
      this.pos.y = height
      this.prevpos = this.pos.copy();
    }
    if(this.pos.y > height)
    {
      this.pos.y = 0
      this.prevpos = this.pos.copy();
    }
  }
}