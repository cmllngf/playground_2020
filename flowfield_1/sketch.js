let seed;
let mapSize = 1000
let xoff = 0
let yoff = 0
let zoff = 0
const numberOfParticles = 50;
let flowfield
const scl = 40
let cols, rows;
const particles = []

function setup() {
  createCanvas(1920, 1080)
  seed = random(99999)
  randomSeed(seed)
  background('black');
  colorMode(HSB, 360,100,100, 1)
  cols = width/scl
  rows = height/scl
  flowfield = new Array(cols*rows)

  for(let i = 0; i < numberOfParticles; i++) {
    particles.push(new Particle('dot', 1000))
  }
  
  yoff = 0
  for (let y = 0; y < rows; y++) {
    xoff = 0;
    for (let x = 0; x < cols; x++) {
      push()
      const phi = noise(xoff,yoff) * TWO_PI
      const v = p5.Vector.fromAngle(phi)
      v.setMag(0.1)
      flowfield[y*cols+x] = v
      translate(x * scl, y * scl)
      rotate(v.heading())
      strokeWeight(1)
      stroke('#13477b')
      // line(0,0, scl, scl)
      pop()
      xoff += 0.06;
    }
    yoff += 0.06;
  }
}

function draw() {
  // background('black');
  for(let i = 0; i < numberOfParticles; i++) {
    particles[i].follow(flowfield)
    particles[i].update()
    particles[i].display()
  }
  zoff+=0.05
  // noLoop()
}

function keyPressed(key) {
  console.log(key)
  if(key.keyCode === 80)
    saveCanvas(canvas, 'squared_'+seed, 'png')
}
