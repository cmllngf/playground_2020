let visited = [];
let colors = [];
let walkers = []
let tries = 0
let stop = false
let x = 0
let y = 0
const amplitude = 0

const k = 300
const pixel_size = 2;
const cwidth = 500;
const cheight = 500;

let t = 0
let shoot

function seed(s, c) {
  visited[s.y * width + s.x] = true;
  colors[s.y * width + s.x] = c;
}

function setup() {
  createCanvas(cwidth, cheight)
  background('white');
  colorMode(HSB)

  // for(let i = 0; i < width * height; i++) {
  //   visited[i] = false;
  //   colors[i] = color(0);
  // }
  shoot = new Shoot(30)
  shootBullet()
  frameRate(30)
  // createLoop({duration:7, gif:true})
}

function shootBullet(
  x1 = int(random(10, width - 10)),
  y1 = int(random(10, height - 10)),
  x2 = int(random(10, width - 10)),
  y2 = int(random(10, height - 10))) 
{
  shoot.shoot(x1, y1, x2, y2)
}

function draw() {
  background('rgba(255,255,255,.4)');
  shoot.update((x,y) => createWalker(x, y, random(50)))
  // noLoop()
  // if(!stop) {
  //   y = int(sin(t * 10) * amplitude + height/2)
  //   createWalker(x, y, -1)
  //   createWalker(cwidth-x, y, -1)
  // }
  if(shoot.justFinished) {
    createWalker(shoot.x2 + 5, shoot.y2 + 5, 50000)
  }

  for(i = 0; i < 300; i++) {
    walkers.forEach((walker) => {
      walker.update(visited, colors, pixel_size)
      if(!walker.alive && walker.lifespan === 50000) {
        visited = [];
        shootBullet(walker.startP.x, walker.startP.y)
      }
    })
  }

  walkers = walkers.filter((walker) => walker.alive)
  t+= .01
  x+= 10
  // y+= 10
}

function createWalker(x = int(random(width)), y = int(random(height)), lifespan = -1) {
  if(tries >= k) {
    stop = true
  }
  if(visited[y * width + x]) {
    tries ++
    return;
  }
  tries = 0
  p = createVector(x, y)
  n = noise(cos(map(x, 0, cwidth/2, 0, TWO_PI)) * 2)
  h = n * 360 * 2
  // h = random(360)
  // s = random(100)
  // b = random(100)
  // s = n * 100
  // s = 30
  s = map(n, 0, 1, 30, 90)
  b = 75
  // b = n * 100
  c = color(h, s, b)
  seed(p, c)
  walkers.push(new Walker(p, lifespan))
}

function keyPressed(key) {
  console.log(key)
  if(key.keyCode === 80)
    saveCanvas(canvas, 'random_walk_color', 'png')
}

