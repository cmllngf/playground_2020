let seed, drawing, px, py, nx, ny;
let stop = false

function setup() {
  createCanvas(1920, 1080)
  background(0)
  seed = random(99999)
  randomSeed(seed)
  drawing = false
  px = random(-width/6, width/6)
  py = random(-height/6, height/6)
  frameRate(10)
}

function draw() {
  translate(width/2, height/2)
  // rotate(random(TWO_PI))
  const steps = [-10,10,-20,20, 0, 0]
  if(!stop) {
    nx = px+steps[floor(random(steps.length))]
    ny = py+steps[floor(random(steps.length - 2))]
  }
  for(let i = 0; i < 12; i++) {
    push()
    rotate((PI/6) * i)
    if(i%2==0)
      scale(-1, 1)
    stroke(255)
    strokeWeight(1)
    if(drawing) {
      line(mouseX-width/2, mouseY-height/2, pmouseX-width/2, pmouseY-height/2)
    }
    if(!stop) {
      line(px, py, nx, ny)
    }
    pop()
  }
  if(!stop) {
    px = nx
    py = ny
  }
}

function keyPressed(key) {
  console.log(key.keyCode)
  if(key.keyCode === 80)
    saveCanvas(canvas, 'no_'+seed, 'png')
  if(key.keyCode === 83)
    stop = !stop
}

function mousePressed() {
  drawing = true
}

function mouseReleased() {
  drawing = false
}
