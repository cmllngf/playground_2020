let THE_SEED;
let xdim = 40;
let ydim = 40;
let size = 16;

let radius = 11;

let cgrid;
let cdimx = 2;
let cdimy = 2;

let grid;
let colors;

function setup() {
  createCanvas(900, 900);
  THE_SEED = floor(random(9999999));
  randomSeed(THE_SEED);
  noLoop();
  fill('#eeeee8');
  stroke('#00eee8');
  strokeWeight(4);
  background('#eeeee8');
};

function draw() {
  translate(width/10, height/10);
  for (let i = 0; i < 1; i++) {
    push();
    for (let j = 0; j < 1; j++) {
      generate_grid(xdim, ydim);
      strokeWeight(10);
      display();
      strokeWeight(4);
      display();
      translate(600, 0);
    }
    pop();
    translate(0, 600);
  }
};

function generate_grid(xd, yd) {
  grid = new Array(yd + 1);
  for (var i = 0; i < grid.length; i++) {
    grid[i] = new Array(xd + 1);
    for (var j = 0; j < grid[i].length; j++) {
      if (i == 0 || j == 0) grid[i][j] = { h: false, v: false, in: false };
      else
        //else if (i == 1 && j == 1) grid[i][j] = { h: true, v: true };
        grid[i][j] = generate_cell(j, i, grid[i][j - 1].h, grid[i - 1][j].v, grid[i][j - 1].in, grid[i - 1][j].in);
    }
  }
}

function generate_cell(x, y, west, north, in_west, in_north) {
  if (!west && !north) {
    if (in_west || in_north) return { h: false, v: false, in: true };
    return flip_temporary_coin(x, y) ? { h: true, v: true, in: true } : { h: false, v: false, in: false };
  }

  if (!west) {
    if (in_west || (!flip_coin() && get_diagonal(x, y, xdim / 2, ydim / 2) < radius))
      return flip_temporary_coin(x, y) ? { h: true, v: true, in: true } : { h: false, v: true, in: in_north };
    return { h: true, v: false, in: false };
  }

  if (!north) {
    if (in_north || (!flip_coin() && get_diagonal(x, y, xdim / 2, ydim / 2) < radius))
      return flip_temporary_coin(x, y) ? { h: true, v: true, in: true } : { h: true, v: false, in: in_west };
    return { h: false, v: true, in: false };
  }

  if (!in_west && !in_north) return { h: false, v: false, in: false };

  let h = flip_fair_coin();
  let v = h ? flip_temporary_coin(x, y) : true;

  let inside = false;
  if (in_west && in_north) inside = !h || !v || flip_temporary_coin(x, y);

  if (v && !in_west) inside = true;
  if (h && !in_north) inside = true;

  return { h, v, in: inside };
}

function display() {
  for (var i = 0; i < grid.length; i++) {
    for (var j = 0; j < grid[i].length; j++) {
      noStroke();
      fill('pink');
      if (grid[i][j].in) rect(j * size, i * size, size, size);
      if (grid[i][j].h) {
        stroke('green');
        line(j * size, i * size, (j + 1) * size, i * size);
      }
      if (grid[i][j].v) {
        stroke('red');
        line(j * size, i * size, j * size, (i + 1) * size);
      }
    }
  }
}

function flip_coin() {
  return random() > 0.9;
}

function flip_temporary_coin(x, y) {
  return flip_coin() && get_diagonal(x, y, xdim / 2, ydim / 2) < radius;
}

function flip_fair_coin() {
  return random() > 0.5;
}

function myDist(n, m) {
  return max(n - m, m - n);
}

function get_diagonal(p1x, p1y, p2x, p2y) {
  return sqrt(pow(myDist(p1x, p2x), 2) + pow(myDist(p1y, p2y), 2));
}

keyPressed = function() {
  if (keyCode === 80) saveCanvas('sketch_' + THE_SEED, 'png');
};