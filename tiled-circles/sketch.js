const radius = 200

let circles = []
let lines = []

function setup() {
  createCanvas(800, 800)
  background(0)

  for(let i = 0; i < 1000; i++) {
    const multiplier = random() > .9 ? 1.5 : 1 
    const x = random(-radius*multiplier, radius*multiplier)
    const y = random(-radius*multiplier, radius*multiplier)
    const d = dist(0,0,x,y)
    if(d > radius/2) {
      // line(0,0,x,y)
      lines.push({
        a: atan2(y, x),
        d,
        speed: random(.001, .01),
        spin: [1,-1][int(random(2))]
      })
    } else {
      const r = radius - d
      // circle(x, y, r)
      circles.push({
        x,y,r
      })
    }
  }
}

function draw() {
  // background(0)
  noFill()
  translate(width/2, height/2)
  circle(0, 0, radius * 2)
  
  for (let i = 0; i < lines.length; i++) {
    const el = lines[i];
    const x = cos(el.a) * el.d
    const y = sin(el.a) * el.d
    stroke(255, 1)
    line(0, 0, x, y)
    lines[i] = {
      ...el,
      a: el.a + el.speed * el.spin
    }
  }
  
  for (let i = 0; i < circles.length; i++) {
    const el = circles[i];
    stroke(0)
    circle(el.x, el.y, el.r)
  }

  // noLoop()
}

function keyPressed(key) {
  if(key.keyCode === 80)
    save()
}

