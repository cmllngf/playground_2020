let seed;
let img1, img2;

function preload() {
  img1 = loadImage('assets/rwc1.png')
  img2 = loadImage('assets/rwc2.png')
}

function setup() {
  createCanvas(img1.width, img1.height)
  seed=random(10000)
  randomSeed(seed)
  background(0);
  img1.loadPixels();
  img2.loadPixels();
}

function draw() {
  let p;
  for (let i = 0; i < 100; i++) {
    const x = floor(random(width))
    const y = floor(random(height))
    if(random() > .5) {
      p = img1.get(x, y)
    } else {
      p = img2.get(x, y)
    }
    noStroke()
    fill(p)
    circle(x, y, 4)
  }
}

function keyPressed(key) {
  if(key.keyCode === 80)
    saveCanvas(canvas, 'noise_noise_'+seed, 'png')
}
