let img;
let system;

function preload() {
  img = loadImage("./god_adam_hands.png");
}

function setup() {
  createCanvas(800, 800);
  background(10);
  // image(img, 0, 0, 800, 800);
  system = new System(20, 20, 760, img);
  colorMode(HSL);
}

function draw() {
  system.update();
}

function keyPressed(key) {
  if (key.keyCode === 80) save();
}
