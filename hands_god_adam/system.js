class System {
  constructor(x, y, size, shader) {
    this.x = x;
    this.y = y;
    this.size = size;
    this.walkers = [];
    this.randomHue = random(360);
    this.visited = [];
    this.colors = [];
    this.pixel_size = 2;
    this.shader = shader;
    this.shader.loadPixels();

    // for (let i = 0; i < 3; i++) {
    //   const wx = int(random(x, x + size));
    //   const wy = int(random(y, y + size));
    //   this.walkers.push(this.createWalker(wx, wy));
    // }
    // this.seed(createVector(408, 430, color(100, 100, 100)));
    this.walkers.push(this.createWalker(408, 430));
  }

  seed(s, c) {
    this.visited[s.y * width + s.x] = true;
    this.colors[s.y * width + s.x] = c;
  }

  //returns false if finished
  update() {
    for (let i = 0; i < 600; i++) {
      this.walkers.forEach((walker) => {
        walker.update(
          this.visited,
          this.colors,
          this.pixel_size,
          color(this.randomHue, 70, 58),
          this.shader
        );
      });
    }
    this.walkers = this.walkers.filter((walker) => walker.alive);
    return this.walkers.length > 0 ? true : false;
  }

  createWalker(x = int(random(width)), y = int(random(height)), lifespan = -1) {
    const p = createVector(x, y);
    const h = random(360);
    const s = random(100);
    const b = random(100);
    const c = color(h, s, b);
    this.seed(p, c);
    return new Walker(
      p,
      lifespan,
      this.x,
      this.x + this.size,
      this.y,
      this.y + this.size
    );
  }
}
