let visited = [];
let colors = [];
let x = 0;
let y = 0;

const k = 300;
const pixel_size = 3;
const cwidth = 800;
const cheight = 800;
const border = 30;
const countH = 1;
const countV = 1;

const poisson_radius = 10;
const poisson_k = 2;

let currentSystemIndex = 0;
const systems = [];

function seed(s, c) {
  visited[s.y * width + s.x] = true;
  colors[s.y * width + s.x] = c;
}

function setup() {
  createCanvas(cwidth, cheight);
  colorMode(HSB);
  background(255);
  for (let x = 0; x < 1; x++) {
    for (let y = 0; y < 1; y++) {
      systems.push(new System(border, border, width - border * 2));
    }
  }
}

function draw() {
  if (!systems[currentSystemIndex].update()) {
    currentSystemIndex++;
    if (!systems[currentSystemIndex]) {
      noLoop();
    }
  }
}

function keyPressed(key) {
  if (key.keyCode === 80) save();
}
