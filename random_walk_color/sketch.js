let visited = [];
let colors = [];
let unprocessed_points = [];

const pixel_size = 2;
const cwidth = 600;
const cheight = 600;
const seedCircleRadius = 100
const seedsNumber = 10;
const circle_radius = 200
const poisson_radius = 40
const poisson_k = 30
const image_path = 'assets/img/image3.jpg'

// Seed creation
const SEED_RANDOM_COLOR = 'seed-rand';
const SEED_IN_CIRCLE = 'seed-circle';
const SEED_IN_DIAG = 'seed-diag';
const SEED_IN_POISSON = 'seed-poisson';
const SEED_IN_BORDER= 'seed-border';
const seedType = SEED_IN_POISSON;

// Propagation constraints
const CONSTRAIN_CIRCLE = 'constrain-circle'
const CONSTRAIN_NORMAL = 'constrain-normal'
const constrainType = CONSTRAIN_NORMAL

// Color origin
const ORIGIN_IMAGE = 'origin-image'
const ORIGIN_RANDOM = 'origin-random'
const originColorType = ORIGIN_RANDOM

// Background shape
const BACKGROUND_CLEAN = 'background-clean'
const BACKGROUND_CIRCLE = 'background-circle'
const backgroundShape = BACKGROUND_CLEAN

let done = false

let img;

function randomLocation() {
  if(seedType === SEED_IN_DIAG) {
    x = int(random(width))
    y = int(map(x, 0, width, height, 0))
    return createVector(x, y);
  } else if(seedType === SEED_IN_CIRCLE) {
    const phi = random(TWO_PI)
    x = int(cos(phi) * seedCircleRadius + width/2)
    y = int(sin(phi) * seedCircleRadius + height/2)
    return createVector(x, y);
  } else if(seedType === SEED_IN_BORDER) {
    
    if(constrainType === CONSTRAIN_CIRCLE) {
      angle = random(TWO_PI)
      x = int(cos(angle) * (circle_radius-1) + width/2)
      y = int(sin(angle) * (circle_radius-1) + height/2)
      return createVector(x, y);
    }

    r = random()
    if(r >= .5) {
      if(r >= .75) 
        return createVector(int(random(width)), 0)
      return createVector(int(random(width)), height-1)
    } else {
      if(r >= .25) 
        return createVector(0, int(random(height)))
      return createVector(width - 1, int(random(height)))
    }
  }
  return createVector(int(random(width)), int(random(height)));
}

/**
 * Returns an array with all the seeds
 */
function seedsLocation() {
  seeds = []
  if(seedType === SEED_IN_POISSON) {
    seeds = poissonDiskSampling(poisson_radius, poisson_k)
  } else {
    for(let i = 0; i < seedsNumber; i++) {
      let p = randomLocation()
      while(!isValid(p)) {
        p = randomLocation()
      }
      seeds.push(p)
    }
  }
  return seeds;
}

/**
 * Return a random color for a seed
 */
function colorSeed() {
  return color(random(255),random(255),random(255))
  // return color(255,random(20),random(20))
  // return color(random(20),random(20),255)
  // return color(random(20),255,random(20))
  // return color(255)
  // return color(0)
}

/**
 * Applies color to seed and adds seed to unprocessed pixels
 * Also marks this seed as visited, so it doesn't get overriden
 */
function seed(s, c) {
  visited[s.y * width + s.x] = true;
  colors[s.y * width + s.x] = c;
  unprocessed_points.push(s)
}

/**
 * Returns the gaussian variation of a given color
 */
function gaussian_color(c) {
  const r = randomGaussian(hue(c), random(1, 3)) + 0
  const g = randomGaussian(saturation(c), random(1, 15)) + 0
  const b = randomGaussian(brightness(c), random(1, 15)) + 0
  return color(r,g,b)
}

/**
 * Returns true if point is within the canvas
 * false if outside
 */
function isValid(p) {
  let isValid = p.x >= 0 && p.x < width && p.y >= 0 && p.y < height
  if(isValid) {
    if(constrainType === CONSTRAIN_CIRCLE) {
      isValid = dist(p.x, p.y, width/2, height/2) < circle_radius
    }
  }
  return isValid;
}

function preload() {
  if(originColorType === ORIGIN_IMAGE) {
    img = loadImage(image_path)
    img.loadPixels()
  }
}

function setup() {
  colorMode(HSB, 255, 255, 255)
  if(originColorType === ORIGIN_IMAGE) {
    createCanvas(img.width, img.height)
    image(img, 0, 0);
  } else {
    createCanvas(cwidth, cheight)
    background('white');
  }
  if(backgroundShape === BACKGROUND_CIRCLE) {
    fill('black')
    noStroke()
    circle(width/2, height/2, circle_radius)
  }

  for(let i = 0; i < width * height; i++) {
    visited[i] = false;
    colors[i] = color(0);
  }

  const seeds = seedsLocation();
  seeds.forEach(seedLocation => {
    let c;
    switch (originColorType) {
      case ORIGIN_IMAGE:
        c = img.get(seedLocation.x, seedLocation.y);
        break;
      default:
        c = colorSeed();
        break;
    }
    seed(seedLocation, c);
  });
}

function draw() {
  for(let oui = 0; oui < 1000; oui++) {
    if(unprocessed_points.length === 0) {
      done = true;
      return;
    }
    const random_index = int(random(unprocessed_points.length));
    const random_point = unprocessed_points[random_index];
    unprocessed_points.splice(random_index, 1);
    // random_point = unprocessed_points.shift()
    // random_point = unprocessed_points.pop()

    for(let y = random_point.y - 1; y <= random_point.y + 1; y++) {
      for(let x = random_point.x - 1; x <= random_point.x + 1; x++) {
        if(y == random_point.y && x == random_point.x) 
          continue;

        if(visited[y * width + x]) 
          continue;
        
        if(!isValid(createVector(x, y)))
          continue;
        
        visited[y * width + x] = true;
        unprocessed_points.push(createVector(x,y))
        if(originColorType === ORIGIN_IMAGE) {
          colors[y * width + x] = gaussian_color(get(random_point.x, random_point.y))
        } else {
          colors[y * width + x] = gaussian_color(colors[random_point.y * width + random_point.x])
        }
        stroke(colors[y * width + x])
        strokeWeight(pixel_size)
        line(random_point.x, random_point.y, x,y)
      }
    }
  }
}

function keyPressed(key) {
  console.log(key)
  if(key.keyCode === 80)
    saveCanvas(canvas, 'random_walk_color', 'png')
}

function isValidPoint(grid, cellsize, gwidth, gheight, p, radius) {
  /* Make sure the point is on the screen */
  if (p.x < 0 || p.x >= width || p.y < 0 || p.y >= height)
    return false;

  /* Check neighboring eight cells */
  xindex = floor(p.x / cellsize);
  yindex = floor(p.y / cellsize);
  i0 = max(xindex - 1, 0);
  i1 = min(xindex + 1, gwidth - 1);
  j0 = max(yindex - 1, 0);
  j1 = min(yindex + 1, gheight - 1);

  for ( i = i0; i <= i1; i++)
    for ( j = j0; j <= j1; j++)
      if (grid[i][j] != null)
        if (dist(grid[i][j].x, grid[i][j].y, p.x, p.y) < radius)
  return false;

  /* If we get here, return true */
  return true;
}

function insertPoint(grid, cellsize, point) {
  xindex = floor(point.x / cellsize);
  yindex = floor(point.y / cellsize);
  grid[xindex][yindex] = point;
}

function poissonDiskSampling(radius, k) {
  N = 2;
  /* The final set of points to return */
  points = []
  /* The currently "active" set of points */
  active = []
  /* Initial point p0 */
  p0 = createVector(int(random(width)), int(random(height)));
  grid = [];
  cellsize = floor(radius/sqrt(N));

  /* Figure out no. of cells in the grid for our canvas */
  ncells_width = ceil(width/cellsize) + 1;
  ncells_height = ceil(width/cellsize) + 1;

  /* Allocate the grid an initialize all elements to null */
  for (i = 0; i < ncells_width; i++) {
    grid[i] = []
    for (j = 0; j < ncells_height; j++)
      grid[i][j] = null;
  }

  insertPoint(grid, cellsize, p0);
  points.push(p0);
  active.push(p0);

  while (active.length > 0) {
    random_index = int(random(active.length));
    p = active[random_index];

    found = false;
    for (tries = 0; tries < k; tries++) {
      theta = int(random(360));
      new_radius = int(random(radius, 2*radius));
      pnewx = int(p.x + new_radius * cos(radians(theta)));
      pnewy = int(p.y + new_radius * sin(radians(theta)));
      pnew = createVector(pnewx, pnewy);

      if (!isValidPoint(grid, cellsize, ncells_width, ncells_height, pnew, radius))
        continue;

      points.push(pnew);
      insertPoint(grid, cellsize, pnew);
      active.push(pnew);
      found = true;
      break;
    }

    /* If no point was found after k tries, remove p */
    if (!found)
      active.splice(random_index, 1);
  }

  return points;
}

