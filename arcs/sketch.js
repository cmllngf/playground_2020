let seed;
const maxIter = 20
const stepsX = [1, 0, 0]
const stepsY = [-1, 1, 0]
let multiplier = 1
let x=1, y=0;

function setup() {
  createCanvas(900, 900)
  seed = random(99999)
  randomSeed(seed)
  background('black');
  // colorMode(HSB, 360,100,100, 1)
}

function draw() {
  x += stepsX[floor(random(stepsX.length))]
  y += stepsY[floor(random(stepsY.length))]
  translate(0, height/2)
  fill(255, 255, 255, 10)
  noStroke()
  push()
  rotate(atan2(y, x))
  arc(10 * multiplier * .10,0 * multiplier,10 * multiplier,10 * multiplier,0,-PI)
  pop()
  console.log('oui')
  multiplier++
  noLoop()
}

function keyPressed(key) {
  if(key.keyCode === 80)
    saveCanvas(canvas, 'arcs_'+seed, 'png')
}

function mouseClicked() {
  loop()
}
