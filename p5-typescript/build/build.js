function setup() {
    createCanvas(windowWidth, windowHeight);
}
function draw() {
    background(0);
    translate(width / 2, height / 2);
    fill(255);
    noStroke();
    circle(0, 0, 50);
}
function windowResized() {
    resizeCanvas(windowWidth, windowHeight);
}
//# sourceMappingURL=../sketch/sketch/build.js.map