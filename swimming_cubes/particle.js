class Particle {
  constructor() {
    this.startY = random(-height/2, height/2)
    this.speedx = random(0.5, 2)
    this.maxRange = random(50, 250)
    this.sizeRange = map(this.maxRange, 50, 250, 1, 5);
    this.size = 10
    this.x = []
    this.y = []
    this.numberOfBodies = 1
    this.colors = []
    for (let i = 0; i < this.numberOfBodies; i++) {
      this.colors[i] = this.color = [random(255), random(255), random(255)];
    }
  }

  update(t) {
    for(let i = 0; i < this.numberOfBodies; i++) {
      this.x[i] = sin(t * this.speedx - i * 0.1) * this.maxRange;
      this.y[i] = this.startY + cos(t * - this.speedx + i*0.01 ) * 100 + i * 2;
    }
  }

  display() {
    noStroke()
    const s = this.size + map(cos(t * this.speedx), -1, 1, -this.sizeRange, this.sizeRange)
    for (let i = 0; i < this.numberOfBodies; i++) {
      fill(this.colors[i][0], this.colors[i][1], this.colors[i][2])
      square(this.x[i], this.y[i], s - i * 2)
    }
  }
}