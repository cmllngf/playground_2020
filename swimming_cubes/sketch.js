let xoff = 0;
let yoff = 0;
let t = 0

particles = [];

function setup() {
  createCanvas(1920, 1080)
  colorMode(RGB);
  for(var i = 0; i < 50; i++){
    particles[i] = new Particle();
  }
}

// cubes running in circle
function draw() {
  background(100, 150, 120);
  translate(width/2, height/2);
  particles.forEach(particle => {
    particle.update(t);
    particle.display();
  });
  t += 0.05
}