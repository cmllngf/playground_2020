let img;
let circles = [];
let seed;
let canvas;
const maxD = 3,
  maxTries = 170;
let currentTries = 0;
const border = 20;
let palette = ["#D5B5F3", "#75D8EC", "#F3A9C5", "#A9F3D3", "#F3C4A9"];
let palette2 = ["#FF6780", "#2B4D4E"];

function preload() {
  img = loadImage("./god_adam_hands.png");
}

function setup() {
  seed = random(9999);
  canvas = createCanvas(800, 800);
  img.loadPixels();
  createNewCircle();
}

function draw() {
  for (let i = 0; i < 6; i++) {
    background(0);
    drawBG2();
    // image(img, 0, 0, 800, 800);
    for (let i = 0; i < circles.length; i++) {
      const c = circles[i];
      if (c.growing) c.update(circles, img);
      c.display();
    }

    createNewCircle();
  }
  noStroke();
  fill(255);
  rect(0, 0, border, height);
  rect(0, 0, width, border);
  rect(width - border, 0, border, height);
  rect(0, height - border, width, border);
}

function drawBG1() {
  push();
  translate(width / 2, height / 2);
  for (let i = 0; i < palette.length; i++) {
    noStroke();
    fill(palette[i]);
    const size = map(i, 0, palette.length, width * 1.5, 50);
    circle(0, 0, size);
  }
  pop();
}

function drawBG2() {
  push();
  translate(width / 2, height / 2);
  // const c1 = color("#00132B");
  // const c2 = color("#30173F");
  // const c1 = color("#561705");
  // const c2 = color("#E69513");
  // const c2 = color("#D5B5F3");
  // const c2 = color("#A9F3D3");
  const c1 = color("#FEFDE9");
  const c2 = color("#FFDBAE");
  const n = 10;
  for (let i = 0; i < n; i++) {
    noStroke();
    const c = lerpColor(c1, c2, i / n);
    fill(c);
    const size = map(i, 0, n, width * 1.5, 50);
    circle(0, 0, size);
  }
  pop();
}

const createNewCircle = () => {
  if (currentTries >= maxTries) {
    noLoop();
    return;
  }

  let running = true;
  while (running && currentTries < maxTries) {
    const x = int(random(width));
    const y = int(random(height));
    const inShader = areCoordInShader(x, y);
    if (circles.some((c) => c.isIn(x, y, inShader ? 1 : 10))) currentTries++;
    else {
      currentTries = 0;
      running = false;
      circles.push(
        new Circle(x, y, getSizeFromShader(x, y), inShader, int(random(1, 5)))
      );
    }
  }
};

const getSizeFromShader = (x, y) => {
  return areCoordInShader(x, y) ? random(10, 15) : random(100, 300);
};

const areCoordInShader = (x, y) => {
  return img.pixels[(y * img.width + x) * 4 + 4] > 0;
};

function keyPressed(key) {
  if (key.keyCode === 80) saveCanvas(canvas, `packed_circle_${seed}`, "png");
}
