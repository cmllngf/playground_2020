class Circle {
  constructor(x, y, maxSize, inShader, step = 1) {
    this.x = x;
    this.y = y;
    this.maxSize = maxSize;
    this.currentSize = 0;
    this.growing = true;
    this.inShader = inShader;
    this.step = step;
    // this.c = palette2[int(random(palette2.length))];
    this.c = this.x > 400 - randomGaussian(0, 120) ? palette2[0] : palette2[1];
    this.filled = random() > 0.7;
  }

  display() {
    noFill();
    if (!this.inShader) {
      stroke(255);
      stroke(this.c);
      stroke(0, 0);
    } else {
      stroke(155, 50, 50);
      stroke(0);
      stroke(255);
      stroke("#2B4D4E");
      stroke(this.c);
      if (this.filled) fill(this.c);
    }
    circle(this.x, this.y, this.currentSize);
  }

  update(circles, shader) {
    shader.loadPixels();
    if (this.growing) {
      this.currentSize += this.step;
      if (this.currentSize >= this.maxSize) this.growing = false;

      if (this.stepsInShader(shader) && !this.inShader) this.growing = false;
      if (this.stepsOutShader(shader) && this.inShader) this.growing = false;
      for (let i = 0; i < circles.length; i++) {
        const c = circles[i];
        if (this.x === c.x && this.y === c.y) continue;

        if (
          this.currentSize / 2 + c.currentSize / 2 + maxD >
          dist(this.x, this.y, c.x, c.y)
        )
          this.growing = false;
      }
    }
  }

  isIn(x, y, d = maxD) {
    return dist(x, y, this.x, this.y) <= this.currentSize / 2 + d;
  }

  stepsOutShader(shader) {
    const step = 100;
    for (let i = 0; i < step; i++) {
      const a = (TWO_PI / step) * i;
      const x = int(cos(a) * (this.currentSize / 2 + maxD) + this.x);
      const y = int(sin(a) * (this.currentSize / 2 + maxD) + this.y);
      // console.log(shader.pixels[(y * width + x) * 4 + 4], "out");
      if (shader.pixels[(y * width + x) * 4 + 4] == 0) return true;
    }
    return false;
  }

  stepsInShader(shader) {
    const step = 100;
    for (let i = 0; i < step; i++) {
      const a = (TWO_PI / step) * i;
      const x = int(cos(a) * (this.currentSize / 2 + maxD) + this.x);
      const y = int(sin(a) * (this.currentSize / 2 + maxD) + this.y);
      // circle(x, y, 2);
      // console.log(shader.pixels[(y * width + x) * 4 + 4], "in");
      if (shader.pixels[(y * width + x) * 4 + 4] > 0) return true;
    }
    return false;
  }
}
