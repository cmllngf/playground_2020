let seed;
let initLen = 100
const maxIter = 10

function setup() {
  createCanvas(900, 900)
  background(0)
  seed = random(99999)
  randomSeed(seed)
}

function draw() {
  // resetMatrix()
  translate(width/2, height)
  branch(initLen, 0)
  noLoop()
}

function branch(len, iter) {
  if(iter >= maxIter)
    return;
  stroke(255)
  noFill()
  line(random(-2,2),random(-2,2),random(-2,2),-len)
  translate(0, -len)
  push()
  rotate(5*PI/6 + random(-PI/6, PI/12))
  branch(-len*.8, iter+1)
  pop()
  push()
  rotate(-5*PI/6 + random(-PI/6, PI/12))
  branch(-len*.8, iter+1)
  pop()
}


function keyPressed(key) {
  if(key.keyCode === 80)
    saveCanvas(canvas, 'tree_'+seed, 'png')
}
