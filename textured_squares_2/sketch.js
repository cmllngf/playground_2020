let img
let seed
const offset = 0

function preload() {
  img = loadImage('assets/img/rwc3.png')
}

function setup() {
  createCanvas(img.width + offset, img.height + offset)
  seed = random(99999)
}

function draw() {
  background(0)
  // image(img,0,0)
  // fillWithSquareAndRotation(50)
  fillWithSquareVaryingSize(50)
  randomSeed(seed)
  noLoop()
}

function fillWithSquareVaryingSize(squareSize) {
  startSize = squareSize
  for(let x = 0; x < width; x+=squareSize-1) {
    squareSize = map(x, 0, width, startSize, 1)
    if(squareSize < 2)
      return
    for(let y = 0; y < height; y+=squareSize-1) {
      push()
      noStroke()
      fill(img.get(x, y))
      translate(x, y)
      square(0, 0, squareSize)
      pop()
    }
  }
}

function fillWithSquareAndRotation(squareSize) {
  cols = floor(width / squareSize)
  rows = floor(height / squareSize)
  marginX = (width % squareSize) / (cols * 2)
  marginY = (height % squareSize) / (rows * 2)
  for(let y = 0; y < rows; y++) {
    for(let x = 0; x < cols; x++) {
      push()
      noStroke()
      fill(img.get(x*squareSize + (squareSize/2), y*squareSize + (squareSize/2)))
      const sx = x*squareSize + marginX*x + marginX*(cols/2) + (offset/2)
      const sy = y*squareSize + marginY*y + marginY*(rows/2) + (offset/2)
      translate(sx, sy)
      maxAngleRange = map(x*y, 0, cols*rows, 0, PI)
      rotate(random(maxAngleRange))
      square(0, 0, squareSize)
      pop()
    }
  }
}

function keyPressed(key) {
  console.log(key)
  if(key.keyCode === 80)
    saveCanvas(canvas, 'squared2_'+seed, 'png')
}
