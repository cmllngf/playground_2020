const opts = {
  width: 700,
  height: 700
};

let dots = []
let planetDots = []
let t = 0

const planetRadius = 100

function setup() {
  createCanvas(opts.width, opts.height)
  colorMode(HSB)
  background(0);
  let numberDots = 50
  for(let j = 0; j < 5; j++){
    dots[j] = []
    for(let i = 0; i < numberDots; i++) {
      dots[j].push({
        centerX: width/2,
        centerY: height/2,
        a: (TWO_PI / numberDots) * i,
        size: int(random(3, 7)),
        radius: planetRadius + 20 + (j + 1) * 20,
        color: random(100, 255),
        speed: random(0.001, .01)
      })
    }
    numberDots *= 1.2
  }

  for (let i = 0; i < 5000; i++) {
    let a = random(TWO_PI)
    let y = random(-1, 1)
    let r = sqrt(1-y*y)
    let z = sin(a)
    const n = noise(cos(a)*planetRadius*r*.01, y*planetRadius+z*r*5*.01)
    planetDots.push({
      a,
      y,
      r,
      c: n > .5 ? color(0, 70, 58) : color(100, 70, 58)
    })
    // a -= t/4
  }
}

function draw() {
  background(0)
  randomSeed(8327638476)
  noStroke()
  fill(50)
  // circle(width/2, height/2, planetRadius*2)
  
  for (let i = 0; i < planetDots.length; i++) {
    const a = planetDots[i].a
    planetDots[i].a -= t
    const dot = planetDots[i]
    let z = sin(dot.a)
    fill(dot.c)
    if (z>0) {
      circle(cos(dot.a)*planetRadius*dot.r + width/2, dot.y*planetRadius+z*dot.r*5 + height/2, 3)
    }
    planetDots[i].a = a
  }

  for (let j = 0; j < dots.length; j++) {
    for (let i = 0; i < dots[j].length; i++) {
      const dot = dots[j][i];
      // const phiJ = (TWO_PI / dots.length) * j; 
      // const phi = (TWO_PI / dots[j].length) * i; 
      // const n = map(noise(cos(phiJ), sin(phi), t), 0, 1, -100, 10)

      const x = cos(dot.a) * dot.radius + dot.centerX
      const y = sin(dot.a) * (dot.radius * .4) + dot.centerY

      // const n = noise(cos(x * .005), sin(y * .005))
      const n = .5
      const nPos = map(n, 0, 1, -40, 40)


      noStroke()
      fill(dot.color)
      if(dist(x + nPos, y + nPos, width/2, height/2) > 100 || y + nPos > height/2)
        circle(x + nPos, y + nPos, dot.size)
      dots[j][i] = {
        ...dot,
        // a: dot.a + (j%2 == 0 ? -.01 : .01),
        a: dot.a + dot.speed,
        size: dot.size
      }
    }
  }
  t+=0.01
}

function keyPressed(key) {
  if(key.keyCode === 80)
    save()
}
