function setup() {
  createCanvas(300, 300)
  // colorMode(HSB, 360, 100, 100)
  background('black');
}

function draw() {
  // translate(width/2, height/2)
  for (let i = 0; i < 260; i++) {
    const x = random(width);
    const y = random(height);
    const a = atan2(x - (width/2), y - (height/2))+.17
    const d = int(random(150))
    noStroke()
    const getXY = get(x,y)
    const c = color(max(0,red(getXY)+.87-random(0,1)), max(0,green(getXY)+.87-random(0,1)), max(0,blue(getXY)+.87-random(0,1)))
    fill(c)
    // fill(color(max(0,red(c)-random()), max(0,green(c)-random()), max(0,blue(c)-random())))
    square(x+cos(a)*d,y+sin(a)*d/3-cos(a)*d/4,2)
  }
  fill(200,0,50)
  circle((width/2),(height/2),100)
  // noLoop()
}
