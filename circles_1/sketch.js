function setup() {
  createCanvas(1000, 1000)
  background(0)
  colorMode(HSB)
}

function draw() {
  translate(width/2, height/2)
  for(let k = 0; k < 5; k++) {
    rotate(PI/10)
    stroke(random(255))
    smooth()
    fill(random(360), 30, 70)
    for(let j = 0; j < 5; j++) {
      rotate(TWO_PI/5)
      for(let i = 0; i < 150; i++) {
        push()
        rotate(TWO_PI/150 * i)
        circle(i*2 + 50,i*2 + 50, map(i, 0, 20, 60, 10, true))
        pop()
      }
    }
  }


  stroke(0)
  strokeWeight(5)
  line(-width, width, height, -height)
  noLoop()
}
