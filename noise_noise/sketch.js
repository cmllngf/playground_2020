let seed;
let zoff = 0

const scl = 1
const noiseMultiplier = 255;
const xstep = 0.01
const ystep = 0.01

function setup() {
  createCanvas(900, 900)
  seed=random(10000)
  randomSeed(seed)
}

function draw() {
  background(0);

  for (let y = 0; y < height; y+=scl) {
    for (let x = 0; x < width; x+=scl) {
      fill(noise(noise(noise(x * xstep, y * ystep) * 2, zoff) * (width/scl)) * noiseMultiplier)
      noStroke()
      // point(x, y)
      square(x, y, x+scl, y+scl)
    }
  }

  noLoop()
  // zoff += 0.01
}

function keyPressed(key) {
  if(key.keyCode === 80)
    saveCanvas(canvas, 'noise_noise_'+seed, 'png')
}
