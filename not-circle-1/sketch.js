let hue, seed, t = 0, moreStep = .000001, step = 0
function setup() {
  createCanvas(400, 650)
  colorMode(HSB)
  hue = random(360)
  seed = random(10000)
  frameRate(30)
  createLoop({duration:7, gif:true})
}

function draw() {
  randomSeed(seed)
  background(hue, 40, 20)
  for(let i = 1; i < 30; i++) {
    const points = random(50, 80)
    step += moreStep
    const angleStep = random(TWO_PI/60, TWO_PI/20) + step
    for(let j = 0; j < points; j++) {
      stroke(hue, 90, 100)
      strokeWeight(random(1, 3) + map(noise(t, i, j), 0, 1, -1, 2))
      const r = 40
      if(i % 2 == 0)
        point(cos(angleStep * j) * i * r + width/2, sin(angleStep * j) * i * r + height/2 + i * 20) 
      else
        point(sin(angleStep * j) * i * r + width/2, cos(angleStep * j) * i * r + height/2 + i * 20)
    }
  }
  t += .01
  // noLoop()
}