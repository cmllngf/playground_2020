//const scl = 0.001;
//const zstep = 0.001;
//const cellSize = 15;
const points = [];
const centers = [];
const numPoints = 100000;
const record = false;
const filePrefix = 'gravityc2';
const frames = 360;

let running = true;
let canvas;
let p;
let frame = 1;

function setup() {
  canvas = createCanvas(400, 400);
  p = createP();
  background(60);
  const inc = height / numPoints;
  for (let i = 0; i < numPoints; i++) {
    const p = new Particle();
    points.push(p);
  }
  grav = createVector(0, -1);
  grav.setMag(height / 4);
  const ctr = createVector(width / 2, height / 2);
  ctr.add(grav);
  gravX = ctr.x;
  gravY = ctr.y;
  
  centers.push(createVector(width / 2, height / 2));
  centers.push(createVector(width / 3, height / 3));
  centers.push(createVector(2 * width / 3, height / 3));
  centers.push(createVector(width / 3, 2 * height / 3));
  centers.push(createVector(2 * width / 3, 2 * height / 3));
  
  if (record) {
    frameRate(1);
  }
}

function draw() {
  if (!running) {
    return;
  }
  background(60);

  for (const p of points) {
    p.follow();
    p.update();
    p.show();
  }
  
  const ctr = createVector(width / 2, height / 2);
  grav.rotate(TWO_PI / 360);
  ctr.add(grav);
  gravX = ctr.x;
  gravY = ctr.y;
  
  /*
  stroke(220);
  fill(220);
  for (const c of centers) {
    circle(c.x, c.y, 5);
  }
  noFill();
  circle(width / 2, height / 2, width / 2);*/
  
  p.html(frameRate().toFixed(2) + ' ' + frameCount);
  
  if (record) {
    recordFrame();
    if (frame === frames) {
      noLoop();
    }
  }
}

function mouseClicked() {
  running = !running;
}

function recordFrame() {
  const fileName = `${filePrefix}-${frame.toString().padStart(3, '0')}.png`;
  saveCanvas(canvas, fileName, 'png');
  frame++;
}





// let seed;

// const noiseMultiplier = 255;
// const xstep = 0.01
// const ystep = 0.01

// let values = []

// function setup() {
//   createCanvas(400, 400)
//   seed=random(10000)
//   randomSeed(seed)
  
//   for (let y = 0; y < height; y++) {
//     for (let x = 0; x < width; x++) {
//       values.push(noise(x * xstep, y * ystep) * noiseMultiplier)
//     }
//   }
// }

// function draw() {
//   background(0);
//   for (let y = 0; y < height; y++) {
//     for (let x = 0; x < width; x++) {
//       const v = (values[y * width + x] % 25) * 5
//       // if(v > 200 && v < 210){
//         stroke(v)
//         point(x, y)
//       // }
//     }
//   }

//   noLoop()
// }

function keyPressed(key) {
  if(key.keyCode === 80)
    saveCanvas(canvas, 'noise_noise_'+seed, 'png')
}
