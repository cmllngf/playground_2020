function setup() {
  createCanvas(windowWidth, windowHeight)
  background('#5cdb95')
}

function draw() {
  noLoop()
  translate(width/2, height/2)

  const countH = 20
  const countV = 20

  const lengthH = 700
  const lengthV = 700

  const maxLineLength = 30

  for (let i = 0; i < countH; i++) {
    for (let j = 0; j < countV; j++) {
      const x = map(i, 0, countH-1, -lengthH/2, lengthH/2)
      const y = map(j, 0, countV-1, -lengthV/2, lengthV/2)
      const d = dist(0,0,x,y)
      const l = d < 300 ? map(d, 0, 300, maxLineLength, 1) : random(maxLineLength, 1)

      if(random() < .1)
      {
        drawLineAlt(x,y, l, maxLineLength)
      } else {
        drawLineBis(x,y, l, maxLineLength)
        drawLine(x,y, l, maxLineLength)
      }
    }
  }

}

function drawLine(x, y, length, maxLength) {
  strokeWeight(5)
  strokeCap(ROUND)
  stroke('#05386b')

  const diffLength = maxLength - length

  line(x,y+diffLength/2,x,(y+diffLength/2)+length)
}

function drawLineBis(x, y, length, maxLength) {
  strokeWeight(5)
  strokeCap(ROUND)
  stroke('#379683')

  const diffLength = maxLength - length

  line(x+3,y+diffLength/2+3,x+3,(y+diffLength/2)+length+3)
}

function drawLineAlt(x, y, length, maxLength) {
  strokeWeight(5)
  strokeCap(ROUND)
  stroke('#EDF5E1')

  const diffLength = maxLength - length

  line(x,y+diffLength/2,x,(y+diffLength/2)+length)
}

function keyPressed(key) {
  if(key.keyCode === 80)
    save()
}

