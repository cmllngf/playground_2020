const cols = 150
const rows = 270
const tileSize = 5
let zoff = 0
let xoffStart = 0, yoffStart = 0;
let seed

const noiseStep = 0.005
const noiseMultiplierColor = 360
const noiseMultiplierHeight = 255

const seaChance = .4

function setup() {
  createCanvas(1920,1080)
  background('black')
  colorMode(HSB, noiseMultiplierColor, 100, 100)
  noLoop()
  seed = random(999999)
  randomSeed(seed)
}

function draw() {
  for(let y = 0; y < rows; y++) {
    for(let x = 0; x < cols; x++) {
      const tx = y%2 == 0 ? x * tileSize * 3 : (tileSize * 1.5) + x * (tileSize * 3)
      const ty = y * (.86 * tileSize)
      const noiseRes = noise(tx*noiseStep, ty*noiseStep)
      const hue = noiseRes < seaChance ? map(noiseRes, 0, seaChance, 270, 200) : map(noiseRes, seaChance, 1, 140, 0)
      draw_hexa(
        tx,
        ty,
        tileSize,
        map(noiseRes, 0, 1, 0, 80),
        hue
      )
    }
  }
}

function draw_hexa(x, y, radius, h, hue) {
  noStroke()
  smooth()
  // TOP
  fill(color(hue,100,80))
  beginShape()
  vertex(x + radius * cos(PI), y + radius * sin(PI) - h)
  vertex(x + radius * cos(2*PI/3), y + radius * sin(2*PI/3) - h)
  vertex(x + radius * cos(PI/3), y + radius * sin(PI/3) - h)
  vertex(x + radius * cos(0), y + radius * sin(0) - h)
  vertex(x + radius * cos(5*PI/3), y + radius * sin(5*PI/3) - h)
  vertex(x + radius * cos(4 * PI/3), y + radius * sin(4 * PI/3) - h)
  endShape(CLOSE)

  // FRONT FACE
  fill(color(hue,100,40))
  beginShape()
  vertex(x + radius * cos(2*PI/3), y + radius * sin(2*PI/3) - h)
  vertex(x + radius * cos(2*PI/3), y + radius * sin(2*PI/3))
  vertex(x + radius * cos(PI/3), y + radius * sin(PI/3))
  vertex(x + radius * cos(PI/3), y + radius * sin(PI/3) - h)
  endShape(CLOSE)

  // FRONT LEFT FACE
  fill(color(hue,100,60))
  beginShape()
  vertex(x + radius * cos(PI), y + radius * sin(PI) - h)
  vertex(x + radius * cos(PI), y + radius * sin(PI))
  vertex(x + radius * cos(2*PI/3), y + radius * sin(2*PI/3))
  vertex(x + radius * cos(2*PI/3), y + radius * sin(2*PI/3) - h)
  endShape(CLOSE)

  // FRONT RIGHT FACE
  fill(color(hue,100,20))
  beginShape()
  vertex(x + radius * cos(PI/3), y + radius * sin(PI/3) - h)
  vertex(x + radius * cos(PI/3), y + radius * sin(PI/3))
  vertex(x + radius * cos(0), y + radius * sin(0))
  vertex(x + radius * cos(0), y + radius * sin(0) - h)
  endShape(CLOSE)
}

function keyPressed() {
  if (keyCode === 80) saveCanvas('hexa_map_' + seed, 'png');
};
