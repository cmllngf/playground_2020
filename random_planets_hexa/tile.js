class Tile {
  constructor(x, y, size, ...color) {
    this.size = size;
    this.color = color;
    this.x = x;
    this.y = y;
  }

  display() {
    let angle = TWO_PI / 6;
    fill(...this.color)
    beginShape();
    for (let a = -PI/6; a < TWO_PI; a += angle) {
      let sx = this.x + cos(a) * this.size;
      let sy = this.y + sin(a) * this.size;
      vertex(sx, sy);
    }
    endShape(CLOSE);
  }

  setPosition(x, y) {
    this.x = x;
    this.y = y;
  }

  setColor(...c) {
    this.color = c
  }
}