let planets = [];
const nbPlanetsX = 1
const nbPlanetsY = 1
const planetSize = 500

function setup() {
  createCanvas(1500, 900)
  colorMode(HSB, 500, 100, 100)
  for (let i = 0; i < nbPlanetsY; i++) {
    planets[i] = [];
    for (let j = 0; j < nbPlanetsX; j++) {
      planets[i][j] = new Planet(j * planetSize + 50, i * planetSize + 50, planetSize, random(2000));
    }
  }
}

function draw() {
  background(0);
  fill(255,255,255);
  for (let i = 0; i < nbPlanetsY; i++) {
    for (let j = 0; j < nbPlanetsX; j++) {
      planets[i][j].update();
      planets[i][j].display();
    }
  }
  // noLoop()
}
