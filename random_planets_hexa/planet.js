class Planet {
  constructor(x, y, size, seed) {
    this.land = 0;
    this.ocean = 0;
    this.x = x;
    this.y = y;
    this.step = 0.06;
    this.map = [];
    this.mapSize = 50;
    this.seed = seed
    this.size = size
    this.tileSize = this.size / this.mapSize / 1.8;

    let yoff = 0;
    let xoff = 0;

    do {
      this.land = random(500)
      this.ocean = random(500)
    } while(abs(this.land - this.ocean) < 100)


    for (let y = 0; y < this.mapSize; y++) {
      this.map[y] = [];
      xoff = 0;
      for (let x = 0; x < this.mapSize; x++) {
        const tx = y%2 === 0 ? x * (this.tileSize * 1.5) : (x + .5) * (this.tileSize * 1.7);
        const ty = y * (this.tileSize * 1.5);
        const n = noise(xoff, yoff, this.seed)
        const c = n > .5 ? this.land : this.ocean
        this.map[y][x] = {
          tile: new Tile(tx,ty,this.tileSize, c, 70 * n * 2, 60 * n * 2),
          xoff,
          yoff,
          color: c
        }
        xoff += this.step;
      }
      yoff += this.step;
    }
  }

  update() {
    for (let y = 0; y < this.mapSize; y++) {
      for (let x = 0; x < this.mapSize; x++) {
        const xoff = this.map[y][x].xoff + this.step
        const yoff = this.map[y][x].yoff
        const tx = y%2 === 0 ? x * (this.tileSize * 1.7) : (x + .5) * (this.tileSize * 1.7);
        const ty = y * (this.tileSize * 1.7);
        const n = noise(xoff, yoff, this.seed)
        const c = n > .5 ? this.land : this.ocean
        this.map[y][x].tile.setPosition(tx + this.x + 15, ty + this.y + 15)
        this.map[y][x].tile.setColor(c, 70 * n * 2, 60 * n * 2)
        this.map[y][x] = {
          ...this.map[y][x],
          xoff,
          yoff
        }
      }
    }
  }

  display() {
    for (let y = 0; y < this.mapSize; y++) {
      for (let x = 0; x < this.mapSize; x++) {
        this.map[y][x].tile.display()
      }
    }
    noFill()
    stroke(this.ocean, 120, 201, .2)
    strokeWeight(10)
    circle(this.x + this.size/2, this.y + this.size/2, this.size - 39)

    fill('black')
    noStroke()
    beginShape()

    vertex(this.x, this.y)
    vertex(this.x + this.size, this.y)
    vertex(this.x + this.size, this.y + this.size)
    vertex(this.x, this.y + this.size)

    beginContour()
    
    let angle = TWO_PI / 50
    for (let i = TWO_PI; i > 0; i-=angle) {
      vertex(
        cos(i) * (this.size/2 - 20) + this.x + this.size/2,
        sin(i) * (this.size/2 - 20) + this.y + this.size/2)
    }

    endContour()

    endShape(CLOSE)
  }
}