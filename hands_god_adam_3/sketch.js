const nodes = 5;
let network = [];
let img;

const avoidBorder = 50;

const poisson_radius = 10;
const poisson_k = 10;

let t = 0;

function preload() {
  img = loadImage("./god_adam_hands.png");
}

function setup() {
  createCanvas(800, 800);
  background(0);
  img.loadPixels();
  network = poissonDiskSampling(poisson_radius, poisson_k);
}

function draw() {
  background(0);
  for (let i = 0; i < network.length; i++) {
    noStroke();

    // if (img.pixels[(network[i].y * width + network[i].x) * 4 + 4] === 0) {
    //   fill(255);
    // } else {
    fill(255);
    // }
    circle(network[i].x, network[i].y, 10);
    const n = neighbors(network[i], 40);
    for (let j = 0; j < min(n.length, 1); j++) {
      // if (img.pixels[(network[i].y * width + network[i].x) * 4 + 4] === 0) {
      stroke(255);
      strokeWeight(4);
      // } else {
      //   stroke(155);
      //   strokeWeight(2);
      // }
      line(network[i].x, network[i].y, n[j].x, n[j].y);
    }
  }
  t += 0.1;
  noLoop();
}

function neighbors(pos, distMax) {
  const n = [];
  // if (img.pixels[(pos.y * width + pos.x) * 4 + 4] === 0) return [];
  for (let i = 0; i < network.length; i++) {
    if (network[i].x == pos.x && network[i].y == pos.y) continue;

    if (dist(network[i].x, network[i].y, pos.x, pos.y) <= distMax)
      n.push(network[i]);
  }
  return n;
}

function isValidPoint(grid, cellsize, gwidth, gheight, p, radius) {
  if (img.pixels[(p.y * width + p.x) * 4 + 4] === 0) return false;

  /* Make sure the point is on the screen */
  if (
    p.x < avoidBorder ||
    p.x >= width - avoidBorder ||
    p.y < avoidBorder ||
    p.y >= height - avoidBorder
  )
    return false;

  /* Check neighboring eight cells */
  xindex = floor(p.x / cellsize);
  yindex = floor(p.y / cellsize);
  i0 = max(xindex - 1, 0);
  i1 = min(xindex + 1, gwidth - 1);
  j0 = max(yindex - 1, 0);
  j1 = min(yindex + 1, gheight - 1);

  for (i = i0; i <= i1; i++)
    for (j = j0; j <= j1; j++)
      if (grid[i][j] != null)
        if (dist(grid[i][j].x, grid[i][j].y, p.x, p.y) < radius) return false;

  /* If we get here, return true */
  return true;
}

function insertPoint(grid, cellsize, point) {
  xindex = floor(point.x / cellsize);
  yindex = floor(point.y / cellsize);
  grid[xindex][yindex] = point;
}

function poissonDiskSampling(radius, k) {
  N = 2;
  /* The final set of points to return */
  points = [];
  /* The currently "active" set of points */
  active = [];
  /* Initial point p0 */
  // p0 = createVector(
  //   int(random(avoidBorder, width - avoidBorder)),
  //   int(random(avoidBorder, height - avoidBorder))
  // );
  p0 = createVector(100, 400);
  p1 = createVector(700, 400);
  grid = [];
  cellsize = floor(radius / sqrt(N));

  /* Figure out no. of cells in the grid for our canvas */
  ncells_width = ceil(width - (avoidBorder * 2) / cellsize) + 1;
  ncells_height = ceil(height - (avoidBorder * 2) / cellsize) + 1;

  /* Allocate the grid an initialize all elements to null */
  for (i = 0; i < ncells_width; i++) {
    grid[i] = [];
    for (j = 0; j < ncells_height; j++) grid[i][j] = null;
  }

  insertPoint(grid, cellsize, p0);
  points.push(p0);
  active.push(p0);
  insertPoint(grid, cellsize, p1);
  points.push(p1);
  active.push(p1);

  while (active.length > 0) {
    random_index = int(random(active.length));
    p = active[random_index];

    found = false;
    for (tries = 0; tries < k; tries++) {
      theta = int(random(360));
      new_radius = int(random(radius, 2 * radius));
      pnewx = int(p.x + new_radius * cos(radians(theta)));
      pnewy = int(p.y + new_radius * sin(radians(theta)));
      pnew = createVector(pnewx, pnewy);

      if (
        !isValidPoint(grid, cellsize, ncells_width, ncells_height, pnew, radius)
      )
        continue;

      points.push(pnew);
      insertPoint(grid, cellsize, pnew);
      active.push(pnew);
      found = true;
      break;
    }

    /* If no point was found after k tries, remove p */
    if (!found) active.splice(random_index, 1);
  }

  return points;
}

function keyPressed(key) {
  if (key.keyCode === 80) save();
}
