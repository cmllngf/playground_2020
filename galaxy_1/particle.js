class Particle {
  constructor(mass = 10, x = random(width), y = random(height)) {
    this.pos = createVector(x, y)
    this.prevPos = this.pos.copy()
    this.vel = createVector(0,0)
    this.acc = createVector(0,0)
    this.maxSpeed = 2
    this.mass = mass
    this.r = mass * 10
  }
  
  update() {
    this.vel.add(this.acc)
    this.vel.limit(this.maxSpeed);
    this.pos.add(this.vel)
    this.acc.mult(0)
  }

  applyForce(force) {
    const f = p5.Vector.div(force, this.mass)
    this.acc.add(f)
  }
  
  display() {
    stroke(255)
    point(this.pos.x, this.pos.y)
  }
}