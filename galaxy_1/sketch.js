let attractor;
let particles = [];
let angles = [];

function setup() {
  createCanvas(900, 900);
  background("black");
  colorMode(HSB);
  for (let i = 0; i < 12; i++) {
    angles.push((TWO_PI / 6) * i);
  }

  for (let i = 0; i < 1000; i++) {
    const a = angles[int(random(angles.length))];
    particles.push({
      scalar: 300,
      scalarSpeed: random(0.2, 0.8),
      startA: a,
      a: 0,
      offsetX: random(-10, 10),
      offsetY: random(-10, 10),
      h: random(260, 360),
    });
  }
}

function draw() {
  background("black");
  translate(width / 2, height / 2);
  rotate(PI / 6);
  strokeWeight(5);
  for (let i = 0; i < particles.length; i++) {
    if (particles[i].scalar >= 0) {
      stroke(
        color(
          particles[i].h,
          map(particles[i].scalar, 300, 0, 70, 5),
          map(particles[i].scalar, 300, 0, 20, 100)
        )
      );
      point(
        cos(particles[i].startA + particles[i].a) * particles[i].scalar +
          particles[i].offsetX,
        sin(particles[i].startA + particles[i].a) * particles[i].scalar +
          particles[i].offsetY
      );
      particles[i].scalar -= particles[i].scalarSpeed;
      particles[i].a -= 0.01;
    } else {
      particles[i].scalar = 300;
      particles[i].a = 0;
      particles[i].offsetX = random(-10, 10);
      particles[i].offsetY = random(-10, 10);
    }
  }
}
