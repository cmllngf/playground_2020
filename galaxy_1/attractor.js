class Attractor {
    constructor(m, x, y) {
        this.pos = createVector(x, y)
        this.mass = m
        this.G = .4
    }

    display() {
        noStroke()
        fill(255)
        circle(this.pos.x, this.pos.y, 5)
    }

    attract(m) {
        let force = p5.Vector.sub(this.pos, m.pos) 
        let d = force.mag()
        d = constrain(d, 1, 25)

        force.normalize()
        const strength = (this.G * this.mass * m.mass) / (d * d)
        force.mult(strength)
        return force
    }

    steer(m) {
        let dir = p5.Vector.sub(this.pos, m.pos);
        let d = dir.mag()
        dir.setMag(map(d, 0, 50, 0, 4, true))
        let steer = p5.Vector.sub(dir, m.vel)
        steer.limit(.1)
        return steer
    }
}