let seed;
let t = 0;
let tpos = 0;

//Planet
const planetRadius = 550

function setup() {
  createCanvas(1910, 920)
  colorMode(HSB, 360, 100, 100)
  seed = random(1000)
}

function draw() {
  background(0);
  fill(255,255,255);
  randomSeed(seed)
  for (let i = 0; i < 800; i++) {
    let [x,y] = [];
    do {
      [x,y] = [random(width), random(height)];
    } while(dist(width*1.12, height/2, x, y) < planetRadius);
    stroke(255, random(100), random(100))
    point(x, y)
  }

  // belt
  push()
  translate(width/1.6,0)
  for (let i = 0; i < 500; i++) {
    let a = random(TWO_PI)
    stroke(map(noise(a), 0, 1, 300, 400), random(20, 50), random(30, 70))
    let rx = random(-planetRadius*.2,planetRadius*.2)
    let ry = random(-5,5)
    a += t/8;
    let xpos = cos(a) * (planetRadius * 1.5 + rx) + width/2
    let ypos = sin(a) * (planetRadius * .1 + ry)  + height/2
    if (ypos > (height/2) || dist(width/2,height/2, xpos , ypos) > 100)
      point(xpos, ypos)
  }
  pop()

  push()
  translate(width,height/2)
  noStroke()
  fill('black')
  rotate(PI)
  arc(0,0,planetRadius*1.15, planetRadius*1.15, 0, PI)
  pop()
  
  //planet
  push()
  translate(width/1.6,0)
  for (let i = 0; i < 1500; i++) {
    let [a,y] = [random(TWO_PI), random(-1, 1)];
    let r = sqrt(1-y*y);
    // let c = noise(y, t) * 500;
    let c = map(noise(y),0,1,0,100);
    a += t/8;
    let z = sin(a);
    stroke(c, 100, 100)
    if (z>0)
      point(cos(a)*planetRadius*r + width/2, y*planetRadius+z*r*5 + height/2)
  }
  pop()

  // moons
  // for (let j = 0; j < 1; j++) {
  //   let startA = random(TWO_PI)
  //   let yoff = random(-75,75)
  //   let size = random(10,30)
  //   let color = [random(500), 100, 100]
  //   for (let i = 0; i < 399; i++) {
  //     let [a,y] = [random(TWO_PI), random(-1, 1)];
  //     let r = sqrt(1-y*y);
  //     a += t/4;
  //     let z = sin(a);
  //     let zpos = sin(tpos+startA)
  //     stroke(...color)
  //     const xpos = cos(a)*size*r + cos(tpos+startA)*planetRadius*1.5 + width
  //     // const xpos = cos(a)*25*r + cos(tpos*startA)*200 + width/2 //!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  //     const ypos = y*size+z*r*5 + (height/3 +yoff) + zpos * 20
  //     if (z>0 && (zpos > 0 || dist(width/2,height/2, xpos , ypos) > 100))
  //       point(xpos, ypos)
  //   }
  // }

  t += 0.1;
  tpos += 0.03;
}
