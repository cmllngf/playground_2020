let visited = [];
let colors = [];
let walkers = []
let tries = 0
let stop = false
let x = 0
let y = 0
const amplitude = 100

const k = 300
var pixel_size = 2;
const cwidth = 900;
const cheight = 900;

let t = 0
var textures
var shader
var colorAimG

function seed(s, c) {
  visited[s.y * width + s.x] = true;
  colors[s.y * width + s.x] = c;
}

function preload() {
  textures = ['6', '10'].map((val) => loadImage(`./assets/img${val}.jpg`))
  shader = loadImage(`./assets/img12.jpg`)
  // textures = [loadImage(`./assets/wallpaper.png`)]
}

function setup() {
  createCanvas(cwidth, cheight)
  colorMode(HSB)
  textures.forEach(t => t.loadPixels())
  shader.loadPixels()
  background('white');
  colorAimG = random(360)

  for (let i = 0; i < 2; i++) {
    x = int(random(width))
    y = int(random(height))
    createWalker(x, y, -1, textures[0], textures[1], shader)
  }
}

function draw() {
  for(i = 0; i < 200; i++) {
    walkers.forEach((walker) => {
      walker.update(visited, colors, pixel_size)
    })
  }

  walkers = walkers.filter((walker) => walker.alive)
}

function createWalker(x = int(random(width)), y = int(random(height)), lifespan = -1, img, img2, shader) {
  if(tries >= k) {
    stop = true
  }
  if(visited[y * width + x]) {
    tries ++
    return;
  }
  tries = 0
  p = createVector(x, y)
  h = random(360)
  s = 70
  b = 58
  c = color(h, s, b)
  seed(p, c)
  walkers.push(new Walker(p, lifespan, img, img2, shader, colorAimG))
}

function keyPressed(key) {
  if(key.keyCode === 80)
    save()
}

function rulesFromTexture(r, g, b) {
  return rules[r +'-'+ g +'-'+ b] || { step: 1 }
}

const rules = {
  '255-0-0': {
    step: -2
  },
  '0-255-0': {
    step: 2
  },
  '0-0-255': {
    step: 5
  },
  '0-0-0': {
    step: -5
  },
  '255-255-255': {
    step: 0
  },
}

