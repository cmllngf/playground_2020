let visited = [];
let colors = [];
let walkers = []
let tries = 0
let stop = false
let x = 0
let y = 0
const amplitude = 100

const k = 300
const pixel_size = 8;
const cwidth = 900;
const cheight = 900;
let t = 0
var textures
var colorAimG

function seed(s, c) {
  visited[s.y * width + s.x] = true;
  colors[s.y * width + s.x] = c;
}

function preload() {
  textures = ['5','8'].map((val) => loadImage(`./assets/img${val}.jpg`))
  // textures = [loadImage(`./assets/wallpaper.png`)]
}

function setup() {
  createCanvas(cwidth, cheight)
  // colorMode(HSB)
  textures.forEach(t => t.loadPixels())
  background('white');
  colorAimG = random(360)

  // for (let i = 0; i < 20; i++) {
  //   const phi = (TWO_PI / 20) * i
  //   for (let j = 0; j < 4; j++) {
  //     x = int(cos(phi) * 100 * (j+1)) + width/2
  //     y = int(sin(phi) * 100 * (j+1)) + height/2
  //     createWalker(x, y, -1, textures[j % textures.length])
  //   }
  // }
  for (let i = 0; i < 2; i++) {
    x = int(random(width))
    y = int(random(height))
    createWalker(x, y, -1, textures[i % textures.length])
  }
}

function draw() {
  for(i = 0; i < 200; i++) {
    walkers.forEach((walker) => {
      walker.update(visited, colors, pixel_size)
    })
  }

  walkers = walkers.filter((walker) => walker.alive)
}

function createWalker(x = int(random(width)), y = int(random(height)), lifespan = -1, text) {
  if(tries >= k) {
    stop = true
  }
  if(visited[y * width + x]) {
    tries ++
    return;
  }
  tries = 0
  p = createVector(x, y)
  h = random(360)
  s = 70
  b = 58
  c = color(h, s, b)
  seed(p, c)
  walkers.push(new Walker(p, lifespan, text))
}

function keyPressed(key) {
  if(key.keyCode === 80)
    save()
}

function rulesFromTexture(r, g, b) {
  return rules[r +'-'+ g +'-'+ b] || { step: 1 }
}

const rules = {
  '255-0-0': {
    step: -2
  },
  '0-255-0': {
    step: 2
  },
  '0-0-255': {
    step: 5
  },
  '0-0-0': {
    step: -5
  },
  '255-255-255': {
    step: 0
  },
}

