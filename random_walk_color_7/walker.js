class Walker {
    constructor(p, lifespan = -1, text) {
        this.startP = p
        this.unprocessed_points = [p]
        this.alive = true
        this.lifespan = lifespan
        this.life = 0
        this.colorAim = color(random(360), 70, 58)
        this.texture = text
    }

    isValid(p) {
        return p.x >= 0 && p.x < width && p.y >= 0 && p.y < height
    }

    gaussian_colorRGB(c, colorAim, step = 1) {
        const r = randomGaussian(red(c), random(1, 20)) + (colorAim ? red(c) < red(colorAim) ? step : -step : 0)
        const g = randomGaussian(green(c), random(1, 20)) + (colorAim ? green(c) < green(colorAim) ? step : -step : 0)
        const b = randomGaussian(blue(c), random(1, 20)) + (colorAim ? blue(c) < blue(colorAim) ? step : -step : 0)
        return color(r,g,b)
    }

    gaussian_color(c, colorAim, step = 10) {
        const r = randomGaussian(hue(c), random(1, 1)) + (colorAim ? hue(c) < hue(colorAim) ? step : -step : 0)
        const g = randomGaussian(saturation(c), random(1, 1)) + (colorAim ? saturation(c) < saturation(colorAim) ? step : -step : 0)
        const b = randomGaussian(brightness(c), random(1, 1)) + (colorAim ? brightness(c) < brightness(colorAim) ? step : -step : 0)
        return color(r,g,b)
    }

    update(visited, colors, pixel_size) {
        if(this.unprocessed_points.length === 0 ||
            this.lifespan !== -1 && this.life >= this.lifespan) {
            this.alive = false;
            return;
        }
        this.life ++;
        const random_index = int(random(this.unprocessed_points.length));
        const random_point = this.unprocessed_points[random_index];
        this.unprocessed_points.splice(random_index, 1);

        for(let y = random_point.y - 1; y <= random_point.y + 1; y++) {
          for(let x = random_point.x - 1; x <= random_point.x + 1; x++) {
            if(y == random_point.y && x == random_point.x) 
              continue;
    
            if(visited[y * width + x]) 
              continue;
            
            if(!this.isValid(createVector(x, y)))
              continue;
            
            visited[y * width + x] = true;
            this.unprocessed_points.push(createVector(x,y))
            
            const r = this.texture.pixels[(y * this.texture.width + x) * 4]
            const g = this.texture.pixels[(y * this.texture.width + x) * 4 + 1]
            const b = this.texture.pixels[(y * this.texture.width + x) * 4 + 2]

            colors[y * width + x] = color(r,g,b)
            colors[y * width + x] = this.gaussian_colorRGB(
              colors[y * width + x],
              this.colorAim,
              // r > 255 ? 5 : 5
            )
            stroke(colors[y * width + x])
            strokeWeight(pixel_size)
            line(random_point.x, random_point.y, x,y)
          }
        }
    }
}