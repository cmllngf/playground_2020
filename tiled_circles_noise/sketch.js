const rows = 100;
const cols = 100;
const defaultSize = 10;
const ijNoiseMult = 0.1;
let t = 0;

function setup() {
  createCanvas(800, 800);
}

function draw() {
  background(255);

  for (let i = 0; i < cols; i++) {
    for (let j = 0; j < rows; j++) {
      const offset = j % 2 == 0 ? -defaultSize / 2 : 0;
      drawShape(
        i * defaultSize + defaultSize / 2 + offset,
        j * defaultSize * 0.9 - defaultSize,
        map(noise(i * ijNoiseMult, j * ijNoiseMult, t), 0, 1, 0, defaultSize),
        map(noise(i * ijNoiseMult, j * ijNoiseMult, t), 0, 1, 0, PI)
      );
    }
  }
  t += 0.1;
  // noLoop();
}

const drawShape = (x, y, s, r = 0) => {
  noStroke();
  fill(0);

  push();
  translate(x, y);
  rotate(r);
  ellipse(0, 0, s, defaultSize);
  pop();
};

function keyPressed(key) {
  if (key.keyCode === 80) save();
}
