let visited = [];
let colors = [];
let walkers = []
let tries = 0
let stop = false
const amplitude = 0
const border = 0
let x = 0
let y = 0
let sorted = false

const k = 300
const pixel_size = 2;
const cwidth = 450;
const cheight = 450;

let t = 0

let randomHue = 0
const poisson_radius = 40
const poisson_k = 3

var sortedCanvas
var sortedContext

var maxLoops = 100 // Don't overdo this, it's expensive

// Change these numbers to change the processing
let threshold = .49; // Best between -1 and 1
let strength = 0.9; // Don't go above 1-ish
let loops = 0;

function seed(s, c) {
  visited[s.y * width + s.x] = true;
  colors[s.y * width + s.x] = c;
}

function setup() {
  createCanvas(cwidth, cheight)
  colorMode(HSB)
  randomHue = random(360)
  // background(color(randomHue, 30, 50));

  frameRate(30)
  // createLoop({duration:7, gif:true})

  seeds = poissonDiskSampling(poisson_radius, poisson_k)
  seeds.forEach(seed => {
    createWalker(seed.x, seed.y)
  })

  sortedCanvas = document.getElementById("defaultCanvas0");
  sortedContext = sortedCanvas.getContext("2d");
}

function draw() {
  for(i = 0; i < 100; i++) {
    walkers.forEach((walker) => {
      walker.update(visited, colors, pixel_size, color(360 - randomHue, 70, 58))
    })
  }

  walkers = walkers.filter((walker) => walker.alive)
  t+= .01
  x+= 10

  if(walkers.length === 0) {
    iterateSort()
    loops++
    if(loops >= maxLoops) {
      console.log('Finished.')
      sharpen(sortedContext, sortedCanvas.width, sortedCanvas.height, 0.7);
      noLoop()
    }
  }
}

function iterateSort() {
  for (let rowIndex = 0; rowIndex < width; rowIndex ++) {
    for (let columnIndex = 0; columnIndex < height - 1; columnIndex++) {
      processIndexPair(columnIndex * width + rowIndex, columnIndex * width + rowIndex + 1);
    }
  }
}

// Compare and recolor two bitmap indices
function processIndexPair(sourceIndex, targetIndex) {
  if (!compare(sourceIndex, targetIndex)) {
    return;
  }

  // Save values before overwriting
  var oldH = hue(colors[targetIndex]),
    oldS = saturation(colors[targetIndex]),
    oldB = brightness(colors[targetIndex]);

  // Swap them pixels
  setPixel(
    targetIndex,
    hue(colors[sourceIndex]),
    saturation(colors[sourceIndex]),
    brightness(colors[sourceIndex])
  );
  setPixel(sourceIndex, oldH, oldS, oldB);
}

function compare(sourceIndex, targetIndex) {
  var oldTotal =
      hue(colors[targetIndex])
      // saturation(colors[targetIndex]) +
      // brightness(colors[targetIndex])
    newTotal =
      hue(colors[sourceIndex])
      // saturation(colors[sourceIndex]) +
      // brightness(colors[sourceIndex])

  // Which way are we comparing?
  let thresholdInt = Math.floor(Math.pow(threshold, 7) * 3 * 360);
  if (thresholdInt > 0) {
    return oldTotal - newTotal > thresholdInt;
  } else {
    return oldTotal - newTotal < thresholdInt;
  }
}

// Change the color of a pixel in a bitmap with alpha blending
function setPixel(index, h, s, b) {
  var orgH = hue(colors[index]),
    orgS = saturation(colors[index]),
    orgB = brightness(colors[index]);

  // Linear interpolation with a
  const newH = orgH + strength * (h - orgH);
  const newS = orgS + strength * (s - orgS);
  const newB = orgB + strength * (b - orgB);
  // const newH = h;
  // const newS = s;
  // const newB = b;
  // noLoop()
  // console.log(newH, newS, newB)
  colors[index] = color(newH, newS, newB)
  // set(index / width, index % width, colors[index])
  point(int(index / width), index % width, colors[index])
}

function createWalker(x = int(random(width)), y = int(random(height)), lifespan = -1) {
  if(tries >= k) {
    stop = true
  }
  if(visited[y * width + x]) {
    tries ++
    return;
  }
  tries = 0
  p = createVector(x, y)
  n = noise(cos(map(x, 0, cwidth/2, 0, TWO_PI)) * 2)
  // h = n * 360 * 2
  // h = 360 - randomHue
  h = randomHue + random (-15, 15)
  // s = n * 100
  s = map(n, 0, 1, 30, 90)
  b = 75
  // h = random(360)
  s = random(100)
  b = random(100)
  c = color(h, s, b)
  seed(p, c)
  walkers.push(new Walker(p, lifespan))
}

function keyPressed(key) {
  console.log(key)
  if(key.keyCode === 80)
    saveCanvas(canvas, 'random_walk_color', 'png')
}

function isValidPoint(grid, cellsize, gwidth, gheight, p, radius) {
  /* Make sure the point is on the screen */
  if (p.x < border || p.x >= width - border || p.y < border || p.y >= height - border)
    return false;

  /* Check neighboring eight cells */
  xindex = floor(p.x / cellsize);
  yindex = floor(p.y / cellsize);
  i0 = max(xindex - 1, 0);
  i1 = min(xindex + 1, gwidth - 1);
  j0 = max(yindex - 1, 0);
  j1 = min(yindex + 1, gheight - 1);

  for ( i = i0; i <= i1; i++)
    for ( j = j0; j <= j1; j++)
      if (grid[i][j] != null)
        if (dist(grid[i][j].x, grid[i][j].y, p.x, p.y) < radius)
  return false;

  /* If we get here, return true */
  return true;
}

function insertPoint(grid, cellsize, point) {
  xindex = floor(point.x / cellsize);
  yindex = floor(point.y / cellsize);
  grid[xindex][yindex] = point;
}

function poissonDiskSampling(radius, k) {
  N = 2;
  /* The final set of points to return */
  points = []
  /* The currently "active" set of points */
  active = []
  /* Initial point p0 */
  p0 = createVector(int(random(border, width-border)), int(random(border, height-border)));
  grid = [];
  cellsize = floor(radius/sqrt(N));

  /* Figure out no. of cells in the grid for our canvas */
  ncells_width = ceil(width - border * 2/cellsize) + 1;
  ncells_height = ceil(height - border * 2/cellsize) + 1;

  /* Allocate the grid an initialize all elements to null */
  for (i = 0; i < ncells_width; i++) {
    grid[i] = []
    for (j = 0; j < ncells_height; j++)
      grid[i][j] = null;
  }

  insertPoint(grid, cellsize, p0);
  points.push(p0);
  active.push(p0);

  while (active.length > 0) {
    random_index = int(random(active.length));
    p = active[random_index];

    found = false;
    for (tries = 0; tries < k; tries++) {
      theta = int(random(360));
      new_radius = int(random(radius, 2*radius));
      pnewx = int(p.x + new_radius * cos(radians(theta)));
      pnewy = int(p.y + new_radius * sin(radians(theta)));
      pnew = createVector(pnewx, pnewy);

      if (!isValidPoint(grid, cellsize, ncells_width, ncells_height, pnew, radius))
        continue;

      points.push(pnew);
      insertPoint(grid, cellsize, pnew);
      active.push(pnew);
      found = true;
      break;
    }

    /* If no point was found after k tries, remove p */
    if (!found)
      active.splice(random_index, 1);
  }

  return points;
}

// Sharpen function from Mike Cao
function sharpen(ctx, w, h, mix) {
  var x, sx, sy, r, g, b, a;
  var dstOff, srcOff, wt, cx, cy, scy, scx;
  var weights = [0, -1, 0, -1, 5, -1, 0, -1, 0],
    katet = Math.round(Math.sqrt(weights.length)),
    half = (katet * 0.5) | 0,
    dstData = ctx.createImageData(w, h),
    dstBuff = dstData.data,
    srcBuff = ctx.getImageData(0, 0, w, h).data,
    y = h;

  while (y--) {
    x = w;
    while (x--) {
      sy = y;
      sx = x;
      dstOff = (y * w + x) * 4;
      r = 0;
      g = 0;
      b = 0;
      a = 0;

      for (cy = 0; cy < katet; cy++) {
        for (cx = 0; cx < katet; cx++) {
          scy = sy + cy - half;
          scx = sx + cx - half;

          if (scy >= 0 && scy < h && scx >= 0 && scx < w) {
            srcOff = (scy * w + scx) * 4;
            wt = weights[cy * katet + cx];

            r += srcBuff[srcOff] * wt;
            g += srcBuff[srcOff + 1] * wt;
            b += srcBuff[srcOff + 2] * wt;
            a += srcBuff[srcOff + 3] * wt;
          }
        }
      }

      dstBuff[dstOff] = r * mix + srcBuff[dstOff] * (1 - mix);
      dstBuff[dstOff + 1] = g * mix + srcBuff[dstOff + 1] * (1 - mix);
      dstBuff[dstOff + 2] = b * mix + srcBuff[dstOff + 2] * (1 - mix);
      dstBuff[dstOff + 3] = srcBuff[dstOff + 3];
    }
  }

  sortedContext.putImageData(dstData, 0, 0);
}

