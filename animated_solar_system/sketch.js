let t = 0;
let planets = []
let stars = []
const distanceRings = 120
let numerRings = 0
let numberStars = 0
const sunSize = 120
let sunHue = 0

function setup() {
  createCanvas(900, 900)
  colorMode(HSB, 360, 100, 100)
  sunHue = int(random(360))
  numerRings = int(random(3, 7))
  numberStars = numerRings * 180
  for (let i = 0; i < numerRings; i++) {
    planets[i] = new Planet((i + 2) * (distanceRings / 2), int(random(360)), 30, 70)
  }
  for (let i = 0; i < numberStars; i++) {
    do {
      stars[i] = {distanceFromCenter: int(random(distanceRings * numerRings + sunSize)), startAngle: random(-TWO_PI, TWO_PI)}
    } while(stars[i].distanceFromCenter > (distanceRings * numerRings + sunSize)/2);
  }
}

function draw() {
  background(0);
  translate(width/2, height/2);

  //draw stars
  stars.forEach(star => {
    stroke(150)
    point(
      cos(t * 0.02 + star.startAngle) * star.distanceFromCenter,
      sin(t * 0.02 + star.startAngle) * star.distanceFromCenter
      // star.startX,
      // star.startY
    )
  });

  //draw sun
  fill(sunHue, 30, 70)
  noStroke()
  circle(0, 0, sunSize)

  //draw rings and planets
  for(let i = 0; i < numerRings; i++) {
    noFill()
    stroke(...(planets[i].getColor()))
    strokeWeight(2)
    circle(0, 0, (i + 2) * distanceRings)
    planets[i].update(t);
    planets[i].display();
  }
  t += 0.01;
  // noLoop()
}