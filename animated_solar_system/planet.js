class Planet {
  constructor(distanceFromSun, ...color) {
    this.color = color;
    this.speed = random(0.5,1.5);
    this.distanceFromSun = distanceFromSun;
    this.ring = random(3) > 2.5
    const moonNumber = int(random(3))
    this.size = [20, 30, 40][int(random(3))]
    this.moons = []
    for (let i = 0; i < moonNumber; i++) {
      const xy = random(-TWO_PI, TWO_PI)
      this.moons.push({
        x: 0,
        y: 0,
        originAngle: xy,
        speed: random(5,9)
      })
    }
  }

  update(t) {
    this.x = cos(t * this.speed) * this.distanceFromSun;
    this.y = sin(t * this.speed) * this.distanceFromSun;
    this.moons = this.moons.map(moon => {
      return {
        ...moon,
        x: cos(t * moon.speed + moon.originAngle) * (this.size/2 + 10) + this.x,
        y: sin(t * moon.speed + moon.originAngle) * (this.size/2 + 10) + this.y
      }
    })
  }

  display() {
    noStroke();
    fill(0);
    circle(this.x, this.y, this.ring ? this.size + 35: this.size + 15);
    fill(...this.color);
    circle(this.x, this.y, this.size);
    this.moons.forEach(moon => {
      circle(moon.x, moon.y, 5);
    })
    if(this.ring) {
      noFill()
      stroke(...this.color)
      strokeWeight(3)
      circle(this.x, this.y, this.size + 20)
    }
  }

  getColor() {
    return this.color;
  }
}