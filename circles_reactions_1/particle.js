const CMGreyScale = 'greyscale'
const CMGreyOneColor = 'greyOneColor'

const TLine = 'line'
const TCirclesOnPoints = 'circlesOnPoints'

class Particle {
  constructor(
    type = TCirclesOnPoints,
    colorMode = CMGreyOneColor,
    c = [255,105,180, 3],
    pos = createVector(random(width), random(height))
  ) {
    this.type = type;
    this.pos = pos
    this.vel = p5.Vector.random2D()
    this.radius = random(20, 110)
    this.speed = random(.5, 3)
    this.vel.mult(this.speed)
    this.isColored = random() > .94
    this.color = c
    this.colorMode = colorMode
  }
  
  update() {
    this.pos.add(this.vel)
    this.edges()
  }
  
  display() {
    noFill()
    stroke(255, 1)
    circle(this.pos.x, this.pos.y, this.radius)
  }

  edges() {
    if(this.pos.x < 0){
      this.pos.x = width
      this.prevpos = this.pos.copy();
    }
    if(this.pos.x > width)
    {
      this.pos.x = 0
      this.prevpos = this.pos.copy();
    }
    if(this.pos.y < 0)
    {
      this.pos.y = height
      this.prevpos = this.pos.copy();
    }
    if(this.pos.y > height)
    {
      this.pos.y = 0
      this.prevpos = this.pos.copy();
    }
  }

  overlapping(particles) {
    particles.forEach(particle => {
      if(particle.pos.x != this.pos.x || particle.pos.y != this.pos.y) {
        const distance = dist(particle.pos.x, particle.pos.y, this.pos.x, this.pos.y)
        const overlapping = (particle.radius /2 + this.radius/2) >= distance
        
        if(overlapping) {
          if(this.type == TLine) {
            if(this.colorMode == CMGreyOneColor && this.isColored)
              stroke(this.color)
            else
              stroke(map(distance, 130, 0, 255, 0), map(distance, 130, 0, 1, 5))
            strokeWeight(1)
            line(this.pos.x, this.pos.y, particle.pos.x, particle.pos.y)
          } else if(this.type == TCirclesOnPoints) {
            let dx = particle.pos.x - this.pos.x;
            let dy = particle.pos.y - this.pos.y;
            let d2 = dx*dx+dy*dy;
            let rr2 = (this.radius/2) * (this.radius/2);
            let RR2 = (particle.radius/2) * (particle.radius/2);

            if(distance < (this.radius/2) + (particle.radius/2) && distance > abs((this.radius/2) - (particle.radius/2))){	 
              let K = rr2-RR2+d2; 
              let K2 = K * K;
              let h = sqrt(4 * rr2 * d2 - K2);
              let x1 = this.pos.x + (dx * K + dy * h)/(2*d2);
              let x2 = this.pos.x + (dx * K - dy * h)/(2*d2);
              let y1 = this.pos.y + (dy * K - dx * h)/(2*d2);
              let y2 = this.pos.y + (dy * K + dx * h)/(2*d2);
              if(this.colorMode == CMGreyOneColor && this.isColored)
                stroke(this.color)
              else
                stroke(map(distance, 130, 0, 0, 255), map(distance, 130, 0, 10, 200))
              strokeWeight(map(distance, 130, 0, 1, 10))
              // line(x1,y1,x2,y2);
              point(x1, y1)
              point(x2, y2)
            }
          }
        }
      }
    })
  }
}