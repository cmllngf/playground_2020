let seed;
let circles = []
let numberOfCircles = 50

function setup() {
  createCanvas(900, 900)
  seed = random(99999)
  randomSeed(seed)
  background('black');
  for (let i = 0; i < numberOfCircles; i++) {
    // circles.push(new Particle(random() > .5 ? TLine : TCirclesOnPoints))
    circles.push(new Particle(TLine))
  }
}

function draw() {
  // background(0, 100)
  for (let i = 0; i < numberOfCircles; i++) {
    circles[i].update()
    // circles[i].display()
    circles[i].overlapping(circles)
  }
}

function keyPressed(key) {
  if(key.keyCode === 80)
    saveCanvas(canvas, 'arcs_'+seed, 'png')
}
