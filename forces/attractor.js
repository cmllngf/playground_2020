class Attractor {
    constructor(m, x, y) {
        this.pos = createVector(x, y)
        this.mass = m
        this.G = .4
    }

    display() {
        noStroke()
        fill(180)
        circle(this.pos.x, this.pos.y, this.mass*5)
    }

    attract(m) {
        let force = p5.Vector.sub(this.pos, m.pos) 
        let d = force.mag()
        d = constrain(d, 5, 25)

        force.normalize()
        const strength = (this.G * this.mass * m.mass) / (d * d)
        force.mult(strength)
        return force
    }
}