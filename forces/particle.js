class Particle {
  constructor(mass = 10, x = random(width), y = random(height)) {
    this.pos = createVector(x, y)
    this.prevPos = this.pos.copy()
    this.vel = createVector(0,0)
    this.acc = createVector(0,0)
    this.maxSpeed = 4
    this.mass = mass
    this.r = mass * 10
  }
  
  update() {
    this.vel.add(this.acc)
    this.vel.limit(this.maxSpeed);
    this.pos.add(this.vel)
    this.acc.mult(0)
    this.edges()
  }

  applyForce(force) {
    const f = p5.Vector.div(force, this.mass)
    this.acc.add(f)
  }
  
  display(c = color(0, 0, 100, .3)) {
    noStroke()
    fill(c)
    circle(this.pos.x, this.pos.y, 1)
    // circle(this.pos.x, this.pos.y, this.r)
    // stroke(c)
    // line(this.pos.x, this.pos.y, this.prevPos.x, this.prevPos.y)
    // this.prevPos = this.pos.copy()
  }

  target(target) {
    let dir = p5.Vector.sub(target, this.pos);
    let d = dir.mag()
    dir.setMag(map(d, 0, 50, 0, 4, true))
    let steer = p5.Vector.sub(dir, this.vel)
    steer.limit(.1)
    this.applyForce(steer)
  }

  edges() {
    if(this.pos.x < 0){
      this.pos.x = width
      this.prevPos = this.pos.copy()
    }
    if(this.pos.x > width)
    {
      this.pos.x = 0
      this.prevPos = this.pos.copy()
    }
    if(this.pos.y < 0)
    {
      this.pos.y = height
      this.prevPos = this.pos.copy()
    }
    if(this.pos.y > height)
    {
      this.pos.y = 0
      this.prevPos = this.pos.copy()
    }
  }

  isInsideLiquid(l) {
    if(this.pos.x > l.x && this.pos.x < l.x + l.w && this.pos.y > l.y && this.pos.y < l.y + l.h)
      return true
    return false
  }

  dragLiquid(l) {
    let speed = this.vel.mag()
    let dragMagnitude = l.c * speed * speed

    let drag = this.vel.copy()
    drag.mult(-1)
    drag.mult(dragMagnitude)
    this.applyForce(drag)
  }
}