let particleNumber = 50
let particles = []
let p
let attractor

let liquid

let t = 0

const numberTargets = 100
let targets = []
let currentTarget

const symetry = true

let canvasCapture

function setup() {
  createCanvas(1900, 1080)
  background(0)
  colorMode(HSB)
  for (let i = 0; i < particleNumber; i++) {
    // particles.push(new Particle(random(1, 10), random(width), 10))
    particles.push(new Particle(random(1,5), random(width), random(height)))
  }

  liquid = new Liquid(0, 600, 900, 300, 0.3);
  attractor = new Attractor(10, 450, 450)
  p = new Particle(1)

  for (let i = 0; i < numberTargets; i++) {
    const r = random(50, 200)
    // const r = 150
    const x = cos(i * (TWO_PI / numberTargets)) * r + width/2
    const y = sin(i * (TWO_PI / numberTargets)) * r + height/2
    noStroke()
    const c = color(random(360), random(80,100), 80)
    c.setAlpha(.1)
    fill(c)
    // circle(x,y, 5)
    targets.push({
      color: c,
      pos:createVector(x, y)
    })
  }

  currentTarget = targets[int(random(targets.length))]
}

function draw() {
  //background(0)
  // drag()
  // friction()
  // attraction()
  // steer()
  for (let i = 0; i < 15; i++) {
    art1() 
  }
}

function friction() {
  for (let i = 0; i < particleNumber; i++) {
    const c = .1
    const normal = 1
    const frictionMag = c * normal
    const f = particles[i].vel.copy()
    f.mult(-1)
    f.normalize()
    f.mult(frictionMag)

    particles[i].applyForce(f)
    particles[i].target(createVector(mouseX, mouseY))
    particles[i].update()
    particles[i].display()
  }
}

function drag() {
  liquid.display()
  for (let i = 0; i < particleNumber; i++) {
    if(particles[i].isInsideLiquid(liquid)) {
      particles[i].dragLiquid(liquid)
    }

    const gravity = createVector(0, .5 * particles[i].mass);
    particles[i].applyForce(gravity)
    particles[i].update()
    particles[i].display()
  }
}

function attraction() {
  attractor.display()
  for (let i = 0; i < particleNumber; i++) {
    const f = attractor.attract(particles[i])
    particles[i].applyForce(f)
    particles[i].update()
    particles[i].display()
  }
}

function steer() {
  p.target(currentTarget.pos)
  p.update()
  p.display(currentTarget.color)
}

function art1() {
  if(random() < .005) {
    currentTarget = targets[int(random(targets.length))]
  }
  
  // const c = color(noise(t) * 360, 70, 70, .1) 

  for (let i = 0; i < particleNumber; i++) {
    particles[i].target(currentTarget.pos)
    particles[i].update()

    if(symetry) {
      for(let j = 0; j < 12; j++) {
        push()
          translate(width/2, height/2)
          rotate((TWO_PI / 12) * j)
          if(j%2 == 0) scale(-1,1)
          particles[i].pos = createVector(particles[i].pos.x - width/2, particles[i].pos.y - height/2)
          particles[i].prevPos = createVector(particles[i].prevPos.x - width/2, particles[i].prevPos.y - height/2)
          particles[i].display(currentTarget.color)
          // particles[i].display(c)
          particles[i].pos = createVector(particles[i].pos.x + width/2, particles[i].pos.y + height/2)
          particles[i].prevPos = createVector(particles[i].prevPos.x + width/2, particles[i].prevPos.y + height/2)
        pop()
      }
    } else {
      particles[i].display(currentTarget.color)
      // particles[i].display(c)
    }
  }
  t += .1
}

function keyPressed(key) {
  if(key.keyCode === 80)
    save()
}
