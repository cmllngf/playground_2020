let visited = [];
let colors = [];
let walkers = [];
let tries = 0;
let stop = false;
const amplitude = 0;
const border = 0;
let x = 0;
let y = 0;

const k = 300;
const pixel_size = 2;
const cwidth = 800;
const cheight = 800;

let t = 0;

let randomHue = 0;
const poisson_radius = 40;
const poisson_k = 1;

function seed(s, c) {
  visited[s.y * width + s.x] = true;
  colors[s.y * width + s.x] = c;
}

function setup() {
  createCanvas(cwidth, cheight);
  colorMode(HSB);
  // randomHue = random(360);
  randomHue = 200;
  background(color(randomHue, 30, 50));

  frameRate(30);
  // createLoop({duration:7, gif:true})

  seeds = poissonDiskSampling(poisson_radius, poisson_k);
  seeds.forEach((seed) => {
    createWalker(seed.x, seed.y);
  });

  beginShape();
  stroke(255, 0.1);
  noFill();
  let x = random(width);
  let y = random(height);
  const j = 5;
  for (let i = 0; i < 30000; i++) {
    curveVertex(x, y);
    const qx = random(1) < 0.5 ? -1 : 1;
    const qy = random(1) < 0.5 ? -1 : 1;
    x += qx * j;
    y += qy * j;
    x = constrain(x, 0, width);
    y = constrain(y, 0, height);
  }
  endShape();
}

function draw() {
  // if(!stop) {
  //   y = int(sin(t * 10) * amplitude + height/2)
  //   createWalker(x + border, y, -1)
  //   createWalker(cwidth-x - border, y, -1)
  // }

  for (i = 0; i < 100; i++) {
    walkers.forEach((walker) => {
      walker.update(
        visited,
        colors,
        pixel_size,
        // color(360 - randomHue, 70, 58)
        color(randomHue, 70, 58)
      );
    });
  }

  walkers = walkers.filter((walker) => walker.alive);
  t += 0.01;
  x += 10;

  const w = 2;
  stroke(250);
  strokeWeight(w);
  // rect(border-w, border-w, width-border*2+w, height-border*2+w)
}

function createWalker(
  x = int(random(width)),
  y = int(random(height)),
  lifespan = -1
) {
  if (tries >= k) {
    stop = true;
  }
  if (visited[y * width + x]) {
    tries++;
    return;
  }
  tries = 0;
  p = createVector(x, y);
  n = noise(cos(map(x, 0, cwidth / 2, 0, TWO_PI)) * 2);
  // h = n * 360 * 2
  // h = 360 - randomHue
  h = randomHue + random(-15, 15);
  // s = n * 100
  s = map(n, 0, 1, 30, 90);
  b = 75;
  // h = random(360)
  s = random(100);
  b = random(100);
  c = color(h, s, b);
  seed(p, c);
  walkers.push(new Walker(p, lifespan));
}

function keyPressed(key) {
  console.log(key);
  if (key.keyCode === 80) saveCanvas(canvas, "random_walk_color", "png");
}

function isValidPoint(grid, cellsize, gwidth, gheight, p, radius) {
  /* Make sure the point is on the screen */
  if (
    p.x < border ||
    p.x >= width - border ||
    p.y < border ||
    p.y >= height - border
  )
    return false;

  /* Check neighboring eight cells */
  xindex = floor(p.x / cellsize);
  yindex = floor(p.y / cellsize);
  i0 = max(xindex - 1, 0);
  i1 = min(xindex + 1, gwidth - 1);
  j0 = max(yindex - 1, 0);
  j1 = min(yindex + 1, gheight - 1);

  for (i = i0; i <= i1; i++)
    for (j = j0; j <= j1; j++)
      if (grid[i][j] != null)
        if (dist(grid[i][j].x, grid[i][j].y, p.x, p.y) < radius) return false;

  /* If we get here, return true */
  return true;
}

function insertPoint(grid, cellsize, point) {
  xindex = floor(point.x / cellsize);
  yindex = floor(point.y / cellsize);
  grid[xindex][yindex] = point;
}

function poissonDiskSampling(radius, k) {
  N = 2;
  /* The final set of points to return */
  points = [];
  /* The currently "active" set of points */
  active = [];
  /* Initial point p0 */
  p0 = createVector(
    int(random(border, width - border)),
    int(random(border, height - border))
  );
  grid = [];
  cellsize = floor(radius / sqrt(N));

  /* Figure out no. of cells in the grid for our canvas */
  ncells_width = ceil(width - (border * 2) / cellsize) + 1;
  ncells_height = ceil(height - (border * 2) / cellsize) + 1;

  /* Allocate the grid an initialize all elements to null */
  for (i = 0; i < ncells_width; i++) {
    grid[i] = [];
    for (j = 0; j < ncells_height; j++) grid[i][j] = null;
  }

  insertPoint(grid, cellsize, p0);
  points.push(p0);
  active.push(p0);

  while (active.length > 0) {
    random_index = int(random(active.length));
    p = active[random_index];

    found = false;
    for (tries = 0; tries < k; tries++) {
      theta = int(random(360));
      new_radius = int(random(radius, 2 * radius));
      pnewx = int(p.x + new_radius * cos(radians(theta)));
      pnewy = int(p.y + new_radius * sin(radians(theta)));
      pnew = createVector(pnewx, pnewy);

      if (
        !isValidPoint(grid, cellsize, ncells_width, ncells_height, pnew, radius)
      )
        continue;

      points.push(pnew);
      insertPoint(grid, cellsize, pnew);
      active.push(pnew);
      found = true;
      break;
    }

    /* If no point was found after k tries, remove p */
    if (!found) active.splice(random_index, 1);
  }

  return points;
}
