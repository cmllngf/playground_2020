let visited = [];
let colors = [];
let unprocessed_points = [];

const pixel_size = 5;
const cwidth = 500;
const cheight = 500;

const SEED_RANDOM_COLOR = 1;
const SEED_IN_CIRCLE = 2;

const seedsNumber = 2;
const seedType = SEED_IN_CIRCLE;

const circle_radius = 200

/**
 * Returns an array with all the seeds
 */
function seedsLocation() {
  let seeds = []
  for(let i = 0; i < seedsNumber; i++) {
    let p = createVector(int(random(-width/2, width/2)), int(random(-height/2, height/2)));
    while(!isValid(p)) {
      p = createVector(int(random(-width/2, width/2)), int(random(-height/2, height/2)));
    }
    seeds.push(p)
  }
  return seeds;
}

/**
 * Return a random color for a seed
 */
function colorSeed() {
  return color(random(255),random(255),random(255))
}

/**
 * Applies color to seed and adds seed to unprocessed pixels
 * Also marks this seed as visited, so it doesn't get overriden
 */
function seed(s, c) {
  visited[s.y * width + s.x] = true;
  colors[s.y * width + s.x] = c;
  unprocessed_points.push(s)
}

/**
 * Returns the gaussian variation of a given color
 */
function gaussian_color(c) {
  const r = randomGaussian(red(c), random(1,6))
  const g = randomGaussian(green(c), random(1,6))
  const b = randomGaussian(blue(c), random(1,6))
  return color(r,g,b)
}

/**
 * Returns true if point is within the canvas
 * false if outside
 */
function isValid(p) {
  let isValid = p.x >= 0 && p.x < width && p.y >= 0 && p.y < height
  if(isValid && seedType === SEED_IN_CIRCLE) {
    isValid = dist(p.x, p.y, 0, 0) < circle_radius
  }
  return isValid;
}

function setup() {
  createCanvas(cwidth, cheight)
  background('white');
  translate(width/2, height/2)
  if(seedType === SEED_IN_CIRCLE) {
    fill('black')
    noStroke()
    circle(0, 0, circle_radius)
  }

  for(let i = 0; i < width * height; i++) {
    visited[i] = false;
    colors[i] = color(0);
  }

  const seeds = seedsLocation();
  seeds.forEach(seedLocation => {
    let c;
    switch (seedType) {
      case SEED_RANDOM_COLOR:
        c = colorSeed();
        break;
      default:
        c = colorSeed();
        break;
    }
    seed(seedLocation, c);
  });
}

function draw() {
  translate(width/2, height/2)
  for(let oui = 0; oui < 1000; oui++) {
    if(unprocessed_points.length === 0)
      return;
    const random_index = int(random(unprocessed_points.length));
    const random_point = unprocessed_points[random_index];
    unprocessed_points.splice(random_index, 1);
    // random_point = unprocessed_points.shift()
    // random_point = unprocessed_points.pop()

    for(let y = random_point.y - 1; y <= random_point.y + 1; y++) {
      for(let x = random_point.x - 1; x <= random_point.x + 1; x++) {
        if(y == random_point.y && x == random_point.x) 
          continue;

        if(visited[y * width + x]) 
          continue;
        
        if(!isValid(createVector(x, y)))
          continue;
        
        visited[y * width + x] = true;
        unprocessed_points.push(createVector(x,y))
        colors[y * width + x] = gaussian_color(colors[random_point.y * width + random_point.x])
        stroke(colors[y * width + x])
        strokeWeight(pixel_size)
        for(let i = 0; i < 12; i++) {
          push()
          if(i % 2 == 0)
            scale(-1,-1)
          rotate(random((PI/6) * i))
          point(random_point.x,random_point.y)
          pop()
        }
      }
    }
  }
}

function keyPressed(key) {
  if(key.keyCode === 80)
    save()
}
