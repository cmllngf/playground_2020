let seed;
let zoff = 0

const scl = 1
const noiseMultiplier = 255;
let xstep = 0.1
let xBegin = 0.1
const ystep = 0.01

function setup() {
  createCanvas(900, 900)
  seed=random(10000)
  randomSeed(seed)
  xBegin = random(1000)
  xstep = xBegin
}

function draw() {
  background(0);
  stroke(255, 40)
  noFill()
    
  xstep = xBegin
  for (let i = 0; i < 2; i++) {
    beginShape()
    for (let x = 0; x < width; x++) {
      noiseF = myNoise(xstep)
      const py =  noiseF * map(noise(x*.01,xstep), 0, 1, -10, 75)
      // strokeWeight(max(1, noiseF * 4))
      curveVertex(x, height/2 + py)
      xstep+=.1
    }
    endShape()
  }
  xBegin += .1

  // noLoop()
}

function myNoise(value) {
  const p = int(value % 5)
  return pow(sin(value), p)
}

function keyPressed(key) {
  if(key.keyCode === 80)
    saveCanvas(canvas, 'noise_exp3_'+seed, 'png')
}
