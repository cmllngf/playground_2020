const circlesInSquare = 200;
const cols = 20, rows = 20;
const margin = 2
let squareSize
let img
const noiseStep = 0.1
let seed
const hueRanges = [[0,100], [100,200], [50,200], [300,360], [200,250], [0,340]]

function preload() {
  // img = loadImage('assets/img/image1.jpg')
}

function setup() {
  // createCanvas(img.width, img.height)
  createCanvas(900, 900)
  seed = random(99999)
}

function draw() {
  background(0)
  // image(img,0,0)
  randomSeed(seed)
  squareSize = min([(width-50)/cols,( height-50)/rows])
  colorMode(HSB, 360, 100, 100)
  randHueRange = hueRanges[floor(random(hueRanges.length))]
  for(let y = 0; y < rows; y++) {
    for(let x = 0; x < cols; x++) {
      const sx = x*squareSize + margin + 25
      const sy = y*squareSize + margin + 25
      // c = img.get(sx + margin + squareSize/2, sy + margin + squareSize/2)
      noiseV = noise(x * noiseStep, y * noiseStep)
      hue = map(noiseV, 0, 1, randHueRange[0], randHueRange[1])
      c = color(hue, 70, 100)
      offsetRange = map(y*x, 0, rows*cols, 0, 20)
      // offsetRange = 0
      draw_textured_square_with_line_random(sx+random(-offsetRange,offsetRange), sy+random(-offsetRange,offsetRange), squareSize - margin * 2, c, noiseV * 150)
    }
  }
  noLoop()
}

function draw_textured_square_with_circle(x,y,size, col) {
  noFill()
  stroke(col)
  strokeWeight(1)
  square(x, y, size)
  for(let i = 0; i < circlesInSquare; i++) {
    const rx = floor(random(x, x + size))
    const ry = floor(random(y, y + size))
    const r = min([rx - x, ry - y, x + size - rx, y + size - ry])
    circle(rx, ry, r * 2)
  }
}

function draw_textured_square_with_line_random(x,y,size, col, numberOfLines) {
  noFill()
  stroke(col)
  strokeWeight(1)
  previous = createVector(x, y)
  for(let i = 0; i < numberOfLines; i++) {
    p = get_random_point_around_square(x,y,size)
    line(previous.x, previous.y, p.x, p.y)
    previous = p
  }
}
function get_random_point_around_square(x,y,size) {
  const rand = floor(random(16))
  const lx = rand % 2 == 0 ? random(x, x+size) : rand % 4 == 1 ? x+size : x
  const ly = rand % 2 == 1 ? random(y, y+size) : rand % 4 == 0 ? y : y+size
  return createVector(lx, ly)
}

function get_random_point_around_square_cyclic(x,y,size,i) {
  const lx = i % 2 == 0 ? random(x, x+size) : i % 4 == 1 ? x+size : x
  const ly = i % 2 == 1 ? random(y, y+size) : i % 4 == 0 ? y : y+size
  return createVector(lx, ly)
}

function keyPressed(key) {
  console.log(key)
  if(key.keyCode === 80)
    saveCanvas(canvas, 'squared_'+seed, 'png')
}
