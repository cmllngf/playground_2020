let boxes = []
const mapWidth = 100
const mapHeight = 100
const boxSize = 10
let seed;
let xoff = 0;
let zoff = 0;
let timePassed = 0
const seaPercentage = .5
let seaColor
let landColor

function setup() {
  createCanvas(900, 900, WEBGL)
  colorMode(HSB, 360, 100, 100)
  seed=random(10000)
  seaColor = random(360)
  landColor = random(360)
}

function draw() {
  background(0);
  rotateX(-PI/3)
  translate(-(mapWidth*boxSize)/2, -80, -(mapHeight*boxSize)/2)
  randomSeed(seed)
  noStroke()
  directionalLight(0, 0, 100, 10, 100, 0);

  zoff = timePassed
  for (let z = 0; z < mapHeight; z++) {
    xoff = 0
    for (let x = 0; x < mapWidth; x++) {
      push()
      
      translate(x * boxSize * 1, 0, z * boxSize)
      const noiseResult = noise(zoff,xoff)
      const boxHeight = noiseResult > seaPercentage ? map(noiseResult,seaPercentage,1,120,180) : 95;
      const c = noiseResult < seaPercentage ? seaColor : landColor + map(noiseResult, seaPercentage, 1, -20,20)
      fill(c, 30, 60);
      box(boxSize, boxHeight, boxSize);
      
      pop()
      xoff += 0.1
    }
    zoff += 0.1
  }
  // timePassed -= 0.1
}
